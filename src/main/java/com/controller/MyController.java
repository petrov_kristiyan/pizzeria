/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.beans.Cart;
import com.beans.UserProducts;
import com.beans.Pizza;
import com.beans.Product;
import com.beans.User;
import com.service.ProductDB;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Scope("request")
public class MyController {

    @Autowired
    private Cart cart;

    @ModelAttribute("products")
    public ArrayList<Product> getProducts() throws SQLException, ClassNotFoundException {
        return ProductDB.getProducts(null);
    }

    @RequestMapping(value = {"/*"}, method = RequestMethod.GET)
    public ModelAndView wellcome() throws SQLException, ClassNotFoundException {
        ModelAndView mav = new ModelAndView("index");
        Product product = new Pizza();
        ArrayList<Product> listProducts  = product.getProducts();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            String email = user.getEmail();
            cart.setOrderList(Cart.joinCarts(cart, user.getCart()));
            user.setCart(cart);
            if (email != null) {
                mav.addObject("email", email);
                mav.addObject("role",user.getRole());
            }
        }
        mav.addObject("list", listProducts);
        return mav;
    }

    @RequestMapping(value = {"/showAddProduct"}, method = RequestMethod.GET)
    public ModelAndView showAddProduct(@RequestParam("idProduct") int id, @ModelAttribute("products") ArrayList<Product> products) throws SQLException {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("addProduct");
        for (Product p : products) {
            if (p.getId() == id && p instanceof Pizza) {
                mav.addObject("name", p.getName());
                mav.addObject("ingredients", ((Pizza) p).getIngredients());
                mav.addObject("cost", p.getCost());
                mav.addObject("id", p.getId());
            }
        }
        return mav;
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
    public ModelAndView register() throws SQLException {
        ModelAndView mav = new ModelAndView("enter");
        return mav;
    }

    @RequestMapping(value = {"/showEnter"}, method = RequestMethod.GET)
    public ModelAndView showEnter() throws SQLException {
        ModelAndView mav = new ModelAndView("enterTemp");
        return mav;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() throws SQLException {
        ModelAndView mav = new ModelAndView("login");
        return mav;
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
    @ResponseBody
    public String register(@RequestParam("email") String email, @RequestParam("password1") String password1, @RequestParam("password2") String password2) throws SQLException, ClassNotFoundException {
        if (password1.equals(password2)) {
            User user = User.register(email, password1);
            if (user.getUserid() != 0) {
                return "{\"status\":\"ok\"}";
            }
        }
        return null;
    }

    /**
     *
     * @param id
     * @param quantity
     * @param products
     * @return
     */
    @RequestMapping(value = {"/addOrder", "/user/addOrder", "/admin/addOrder"}, method = RequestMethod.POST)
    public @ResponseBody
    String addOrder(@RequestParam("idProduct") int id, @RequestParam("quantityProduct") int quantity, @ModelAttribute("products") ArrayList<Product> products) {
        for (Product p : products) {
            if (p.getId() == id) {
                if (!cart.addOrder(p, quantity)) {
                    return null;
                }
                JsonArrayBuilder jsonArray = Json.createArrayBuilder();
                for (Map.Entry<Integer, UserProducts> entry : cart.getOrderList().entrySet()) {
                    jsonArray.add(Json.createObjectBuilder()
                            .add("id", entry.getValue().getProduct().getId())
                            .add("quantity", entry.getValue().getQuantity())
                            .add("name", entry.getValue().getProduct().getName())
                            .add("cost", entry.getValue().getProduct().getCost()));
                }
                JsonArray value = jsonArray.build();
                return value.toString();
            }
        }
        return null;
    }

    /**
     *
     * @param id
     * @param quantity
     * @param products
     * @return
     */
    @RequestMapping(value = {"/updateOrder", "/user/updateOrder", "/admin/updateOrder"}, method = RequestMethod.POST)
    public @ResponseBody
    String updateOrder(@RequestParam("idProduct") int id, @RequestParam("quantityProduct") int quantity, @ModelAttribute("products") ArrayList<Product> products) {
        for (Product p : products) {
            if (p.getId() == id) {

                cart.updateOrder(p.getId(), quantity);
                JsonArrayBuilder jsonArray = Json.createArrayBuilder();
                for (Map.Entry<Integer, UserProducts> entry : cart.getOrderList().entrySet()) {
                    jsonArray.add(Json.createObjectBuilder()
                            .add("id", entry.getValue().getProduct().getId())
                            .add("quantity", entry.getValue().getQuantity())
                            .add("name", entry.getValue().getProduct().getName())
                            .add("cost", entry.getValue().getProduct().getCost()));
                }
                JsonArray value = jsonArray.build();
                return value.toString();
            }
        }
        return null;
    }

    /**
     *
     * @param id
     * @param quantity
     * @param products
     * @return
     */
    @RequestMapping(value = {"/deleteOrder", "/user/deleteOrder", "/admin/deleteOrder"}, method = RequestMethod.POST)
    public @ResponseBody
    String deleteOrder(@RequestParam("idProduct") int id) {
        if (!cart.removeOrderById(id)) {
            return null;
        }
        JsonArrayBuilder jsonArray = Json.createArrayBuilder();
        for (Map.Entry<Integer, UserProducts> entry : cart.getOrderList().entrySet()) {
            jsonArray.add(Json.createObjectBuilder()
                    .add("id", entry.getValue().getProduct().getId())
                    .add("quantity", entry.getValue().getQuantity())
                    .add("name", entry.getValue().getProduct().getName())
                    .add("cost", entry.getValue().getProduct().getCost()));
        }
        JsonArray value = jsonArray.build();
        return value.toString();
    }

    @RequestMapping(value = {"/checkout"}, method = RequestMethod.GET)
    public String checkout() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getPrincipal().equals("anonymousUser")) {
            return "redirect:/user/checkout";
        } else {
            return "redirect:/login";
        }
    }
}
