/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.beans.Cart;
import com.beans.Order;
import com.beans.Pizza;
import com.beans.Product;
import com.beans.User;
import com.service.ProductDB;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author kristiyan
 */
@Controller
@Secured({"ROLE_ADMIN"})
@RequestMapping("/admin")
public class AdminController {

    String patternName = "[a-zA-Z0-9]{3,}"; //name at least 3 chars
    String patternWordList = "[\\w\\s-]+(,[\\w\\s-]*)*"; // word list comma separated

    @Autowired
    ServletContext servletContext;

    @ModelAttribute("products")
    public ArrayList<Product> getProducts() throws SQLException, ClassNotFoundException {
        return ProductDB.getProducts(null);
    }
    
    @RequestMapping(value = {"/*"}, method = RequestMethod.GET)
    public String wellcome() throws SQLException, ClassNotFoundException {
        return "redirect:/";
    }
    
    @RequestMapping(value = "/index")
    public ModelAndView showTestPage() throws SQLException, ClassNotFoundException {
        ModelAndView mav = new ModelAndView("admin");
        ArrayList<Product> listProducts = new ArrayList<>();
        Product product = new Pizza();
        listProducts = product.getProducts();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            String email = user.getEmail();
            if (email != null) {
                mav.addObject("email", email);
                mav.addObject("role",user.getRole());
            }
        }
        mav.addObject("time", new Date().getTime());
        
        mav.addObject("list", listProducts);
        return mav;
    }

    @RequestMapping(value = {"/showInsertProduct"}, method = RequestMethod.GET)
    public ModelAndView showAddProduct() throws SQLException {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("insertProduct");
        return mav;
    }

    @RequestMapping(value = "/deleteProduct", method = RequestMethod.POST)
    public @ResponseBody
    String deleteProduct(@RequestParam("productId") int id) throws ClassNotFoundException, SQLException {
        if (Product.deleteProduct(id)) {
            return "{\"status\":\"ok\"}";
        }
        return null;
    }
    @RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = "/insertProduct", method = RequestMethod.POST)
    public ModelAndView insertProduct(@RequestParam("file") MultipartFile file, @RequestParam("inputName") String name, @RequestParam("inputIngredients") String ingredients, @RequestParam("inputCategory") String category, @RequestParam("inputDescription") String description, @RequestParam("inputPrice") double price, HttpServletResponse resp) throws ClassNotFoundException, SQLException, IOException {
        String path = servletContext.getRealPath("/resources/img/products/");
        if (file != null) {
            String mimeType = file.getContentType();
            if (name.matches(patternName) && category.matches(patternName) && ingredients.matches(patternWordList) && price >= 0 && mimeType.contains("image/")) {
                Product p = Product.insertProduct(file, category, description, name, ingredients, price, path);
                ModelAndView mav = new ModelAndView();
                mav.setViewName("singleProduct");
                mav.addObject("time", new Date().getTime());
                mav.addObject("product", p);
                return mav;
            }
        }
        resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
        return null;
    }

    @RequestMapping(value = "/editProduct", method = RequestMethod.POST)
    public ModelAndView editProduct(@RequestParam("file") MultipartFile file, @RequestParam("inputId") int id, @RequestParam("inputName") String name, @RequestParam("inputIngredients") String ingredients, @RequestParam("inputCategory") String category, @RequestParam("inputDescription") String description, @RequestParam("inputPrice") double price, HttpServletResponse resp) throws ClassNotFoundException, SQLException, IOException {
        String path = servletContext.getRealPath("/resources/img/products/");
        if (file != null) {
            String mimeType = file.getContentType();
            if (name.matches(patternName) && ingredients.matches(patternWordList) && price >= 0 && mimeType.contains("image/")) {
                ingredients = ingredients.replace(" ", "");
                if (Product.updateProduct(file, category, description, name, ingredients, price, path, id)) {
                    Product p = new Pizza();
                    p.setCategory(category);
                    p.setCost(price);
                    p.setDescription(description);
                    p.setId(id);
                    p.setName(name);
                    ((Pizza) p).setIngredients(new ArrayList<>(Arrays.asList(ingredients.split(","))));
                    ModelAndView mav = new ModelAndView();
                    mav.setViewName("singleProduct");
                    mav.addObject("time", new Date().getTime());
                    mav.addObject("product", p);
                    return mav;
                }
            }
        }
        resp.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
        return null;
    }

    @RequestMapping(value = {"/showEditProduct"}, method = RequestMethod.GET)
    public ModelAndView showEditProduct(@RequestParam("idProduct") int id, @ModelAttribute("products") ArrayList<Product> products) throws SQLException {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("editProduct");
        for (Product p : products) {
            if (p.getId() == id && p instanceof Pizza) {
                mav.addObject("name", p.getName());
                mav.addObject("description", p.getDescription());
                mav.addObject("category", p.getCategory());
                mav.addObject("ingredients", ((Pizza) p).getIngredients().toString().replace("[", "").replace("]", ""));
                mav.addObject("cost", p.getCost());
                mav.addObject("id", p.getId());
            }
        }
        return mav;
    }

    @RequestMapping(value = {"/orders"}, method = RequestMethod.GET)
    public ModelAndView viewOrders() throws SQLException, ClassNotFoundException {
        ModelAndView mav = new ModelAndView("ordersList");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            mav.addObject("user", user);
            ArrayList<Order> orders = Order.getAllOrders();
            mav.addObject("ordersList", orders);
        }
        return mav;
    }

}
