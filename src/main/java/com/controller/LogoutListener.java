/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.beans.Cart;
import com.beans.User;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

/**
 *
 * @author kristiyan
 */
@Component
public class LogoutListener implements ApplicationListener<SessionDestroyedEvent> {

    @Override
    public void onApplicationEvent(SessionDestroyedEvent event)
    {
        List<SecurityContext> lstSecurityContext = event.getSecurityContexts();
        User user;
        for (SecurityContext securityContext : lstSecurityContext)
        {
            user= (User) securityContext.getAuthentication().getPrincipal();
            if(user.getCart().isChanged()){
                try {
                    Cart.saveCart(user);
                } catch (SQLException ex) {
                    Logger.getLogger(LogoutListener.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(LogoutListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
