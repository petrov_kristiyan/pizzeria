/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.beans.Address;
import com.beans.User;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author kristiyan
 */
@Controller
@Secured({"ROLE_USER", "ROLE_ADMIN"})
@RequestMapping("/user")
public class UserController {

    @Autowired
    private Address deliveryAddress;
    private Date deliveryDate;

    @RequestMapping(value = {"/*"}, method = RequestMethod.GET)
    public String wellcome() throws SQLException, ClassNotFoundException {
        return "redirect:/";
    }
    
    @RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = {"/profile"}, method = RequestMethod.GET)
    public ModelAndView showProfile() throws SQLException {
        ModelAndView mav = new ModelAndView("editProfile");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            mav.addObject("user", user);
        }
        return mav;
    }

    @RequestMapping(value = {"/editProfile"}, method = RequestMethod.POST)
    @ResponseBody
    public String editProfile(@RequestParam("name") String name, @RequestParam("surname") String surname, @RequestParam("address") String address, @RequestParam("phone") String phone, @RequestParam(value = "email", required = false) String email, @RequestParam(value = "password1", required = false) String password1, @RequestParam(value = "password2", required = false) String password2) throws SQLException, ClassNotFoundException, NumberParseException {
        if ((password1 != null || password2 != null) && !password1.equals(password2)) {
            return null;
        }
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        PhoneNumber number = phoneUtil.parseAndKeepRawInput(phone, "");
        if (!phoneUtil.isValidNumber(number)) {
            return null;
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            if (user.editProfile(user, name, surname, address, phone, email, password1)) {
                return "{\"status\":\"ok\"}";
            }
        }
        return null;
    }

    @RequestMapping(value = {"/checkout"}, method = RequestMethod.GET)
    public ModelAndView cart() {
        ModelAndView mav = new ModelAndView("checkout");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            deliveryAddress = new Address();
            deliveryDate = null;
            mav.addObject("cart", user.getCart());
        }
        return mav;
    }

    @RequestMapping(value = {"/nextPhaseCheckout"}, method = RequestMethod.POST)
    public ModelAndView userDetailsCheckout() {
        ModelAndView mav = new ModelAndView("checkout_user");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            if ((user.getName() != null && user.getSurname() != null && user.getPhone() != null && user.getAddress().getAddress() != null) && (!user.getName().isEmpty() && !user.getSurname().isEmpty() && !user.getPhone().isEmpty() && !user.getAddress().getAddress().isEmpty())) {
                if (deliveryAddress.getAddress() != null && deliveryDate != null) {
                    mav.addObject("address", deliveryAddress);
                    mav.addObject("deliveryDate", deliveryDate);
                    mav.setViewName("checkout_complete");
                } else {
                    mav.setViewName("checkout_delivery");
                }
            }
            mav.addObject("user", user);
        }

        return mav;
    }

    @RequestMapping(value = {"/deliveryAddress"}, method = RequestMethod.POST)
    @ResponseBody
    public String deliveryAddress(@RequestParam("address") String address, @RequestParam("deliveryDate") String deliveryDate) throws SQLException, ClassNotFoundException {
        if (deliveryDate != null && address != null && isValidDate("yyyy/MM/dd HH:mm", deliveryDate) && !address.isEmpty() && !deliveryDate.isEmpty()) {
            deliveryAddress.setAddress(address);
            return "{\"status\":\"ok\"}";
        }
        return null;
    }

    @RequestMapping(value = {"/completeCheckout"}, method = RequestMethod.GET)
    @ResponseBody
    public String completedCheckout() throws SQLException, ClassNotFoundException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            if (user.saveOrder(user, deliveryAddress.getAddress(), deliveryDate)) {
                return "{\"status\":\"ok\"}";
            }

        }
        return null;
    }

    @RequestMapping(value = {"/deleteOrder"}, method = RequestMethod.GET)
    @ResponseBody
    public String deleteOrder(@RequestParam("order") int id) throws SQLException, ClassNotFoundException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            if (user.deleteOrder(user, id)) {
                return "{\"status\":\"ok\"}";
            }
        }
        return null;
    }

    @RequestMapping(value = {"/confirmDelivery"}, method = RequestMethod.GET)
    @ResponseBody
    public String confirmDelivery(@RequestParam("order") int id) throws SQLException, ClassNotFoundException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            if (user.confirmDelivery(user, id)) {
                return "{\"status\":\"ok\"}";
            }
        }
        return null;
    }

    @RequestMapping(value = {"/orders"}, method = RequestMethod.GET)
    public ModelAndView viewOrders() throws SQLException, ClassNotFoundException {
        ModelAndView mav = new ModelAndView("orders");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User) {
            User user = (User) authentication.getPrincipal();
            mav.addObject("user", user);
        }
        return mav;
    }

    public boolean isValidDate(String format, String value) {
        Date date;
        Date today = new Date();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                return false;
            }
            today = roundUp(today);
            Date date1 = roundUp(date);
            if (today.compareTo(date1) >= 0 && (date1.getHours() < 8 || date1.getHours() >= 23)) {
                return false;
            }
            setDate(date1);
        } catch (ParseException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public void setDate(Date date) {
        this.deliveryDate = date;
    }

    public Date roundUp(Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        if (calendar.get(Calendar.MINUTE) > 30) {
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1);
        } else if (calendar.get(Calendar.MINUTE) > 0 && calendar.get(Calendar.MINUTE) < 30) {
            calendar.set(Calendar.MINUTE, 30);
        }
        return calendar.getTime();
    }

}
