package com.service;

import com.beans.Pizza;
import com.beans.Product;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import org.springframework.web.multipart.MultipartFile;

public class ProductDB {

    public static ArrayList<Product> getProducts(String category) throws SQLException, ClassNotFoundException {
        ArrayList products = new ArrayList<>();
        String query = "SELECT * FROM PRODUCTS";
        PreparedStatement ps = DBManager.createStatement(query);
        try {

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Product product;
                if (rs.getString("CATEGORY").equals("classic")) {
                    product = new Pizza();
                    product.setDescription(rs.getString("DESCRIPTION"));
                    product.setCost(rs.getDouble("COST"));
                    String ingredients = rs.getString("INGREDIENTS");
                    if (ingredients != null) {
                        ArrayList<String> ingredientsList = new ArrayList<String>();
                        Collections.addAll(ingredientsList, ingredients.split(","));
                        ((Pizza) product).setIngredients(ingredientsList);
                    }
                    product.setId(rs.getInt("PRODUCTID"));
                    product.setName(rs.getString("NAME"));
                    product.setCategory(rs.getString("CATEGORY"));
                    product.setCost(rs.getDouble("COST"));
                    products.add(product);
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }

        return products;
    }

    public static boolean deleteProduct(int id) throws ClassNotFoundException, SQLException {
        String query = "UPDATE PRODUCTS SET CATEGORY=? WHERE PRODUCTID=?";
        PreparedStatement ps = DBManager.createStatement(query);
        try {
            ps.setString(1, "DELETED");
            ps.setInt(2, id);
            return ps.executeUpdate() != 1;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
    }

    public static Product insertProduct(MultipartFile file, String category, String description, String name, String ingredients, Double price, String path) throws ClassNotFoundException, SQLException {
        String s = "INSERT INTO PRODUCTS(NAME,INGREDIENTS,COST,CATEGORY,DESCRIPTION) VALUES(?,?,?,?,?)";
        PreparedStatement ps = DBManager.createStatement(s);
        Product p = null;
        int id = 0;
        try {
            ps.setString(1, name);
            ps.setString(2, ingredients);
            ps.setDouble(3, price);
            ps.setString(4, category);
            ps.setString(5, description);
            ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt(1);
                } else {
                    throw new SQLException("problem ID");
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
        String fileName = null;
        if (!file.isEmpty()) {
            try {
                fileName = file.getOriginalFilename();
                byte[] bytes = file.getBytes();
                BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(new File(path, id + ".jpg")));
                buffStream.write(bytes);
                buffStream.close();
                p = new Pizza();
                p.setDescription(description);
                ((Pizza) p).setIngredients(new ArrayList<>(Arrays.asList(ingredients.split(","))));
                p.setId(id);
                p.setName(name);
                p.setCategory(category);
                p.setCost(price);
                return p;
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("Unable to upload. File is empty.");
        }
        return null;
    }

    public static boolean updateProduct(MultipartFile file, String category, String description, String name, String ingredients, Double price, String path,int id) throws ClassNotFoundException, SQLException {
        String s = "UPDATE PRODUCTS SET NAME=?,INGREDIENTS=?,COST=?,CATEGORY=?,DESCRIPTION=? WHERE PRODUCTID=?";
        PreparedStatement ps = DBManager.createStatement(s);
        Product p = null;
        try {
            ps.setString(1, name);
            ps.setString(2, ingredients);
            ps.setDouble(3, price);
            ps.setString(4, category);
            ps.setString(5, description);
            ps.setInt(6, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
        String fileName = null;
        if (!file.isEmpty()) {
            try {
                fileName = file.getOriginalFilename();
                byte[] bytes = file.getBytes();
                BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(new File(path, id + ".jpg")));
                buffStream.write(bytes);
                buffStream.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return false;
            }
        } else {
            System.out.println("Unable to upload. File is empty.");
            return false;

        }
        return true;

    }

}
