package com.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.*;
import com.sun.rowset.CachedRowSetImpl;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.sql.rowset.CachedRowSet;
/**
 * Classe che si occupa del accesso al DB 
 * e delle query
 */
public class DBManager{
    private static String url = "jdbc:mysql://127.0.0.1:3306/pizza?useUnicode=yes&amp;characterEncoding=UTF-8";
    private static String user = "root";
    private static String pass = "v9NOYgp8";

    public DBManager() {
        setDetails();
    }

    public void setDetails() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            String userDir = System.getProperty("user.home");
            input = new FileInputStream(userDir + "/.yousquared.configuration");
            // load a properties file
            prop.load(input);
            // get the property value and set
            String url = prop.getProperty("com.yousquared.server.configuration.jdbc.url");
            String user = prop.getProperty("com.yousquared.server.configuration.jdbc.user");
            String pass = prop.getProperty("com.yousquared.server.configuration.jdbc.password");
            if (url != null && !url.equals("")) {
                this.url = url;
            }
            if (user != null && !user.equals("")) {
                this.user = user;
            }
            if (pass != null && !pass.equals("")) {
                this.pass = pass;
            }
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static PreparedStatement createStatement(String query) throws ClassNotFoundException, SQLException {
        Connection con;
        PreparedStatement ps = null;
        
            con = DriverManager.getConnection(url, user, pass);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
              return ps;      
       
    }
}