/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.service;

import com.beans.Cart;
import com.beans.User;
import com.beans.UserProducts;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author kristiyan
 */
public class CartDB {

    public static void saveCart(User user) throws SQLException, ClassNotFoundException {
        if (user.getUserid() != 0) {
            Cart cart = user.getCart();
            String s = "DELETE FROM CART WHERE USERID=?";
            PreparedStatement ps = DBManager.createStatement(s);
            try {
                    ps.setInt(1, user.getUserid());
                   
                    ps.executeUpdate();

                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                } finally {
                    Connection con = ps.getConnection();
                    ps.close();
                    con.close();
                }
            LinkedHashMap<Integer, UserProducts> ordersList = cart.getOrderList();
            for (Map.Entry<Integer, UserProducts> entry : ordersList.entrySet()) {  
                s = "INSERT INTO CART ( PRODUCTID, USERID,QUANTITY)VALUES(?,?,?)";
                ps = DBManager.createStatement(s);
                try {
                    ps.setInt(1, entry.getValue().getProduct().getId());
                    ps.setInt(2, user.getUserid());
                    ps.setInt(3, entry.getValue().getQuantity());
                    ps.executeUpdate();

                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                } finally {
                    Connection con = ps.getConnection();
                    ps.close();
                    con.close();
                }
            }
        }
    }
}
