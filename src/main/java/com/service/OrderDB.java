/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.service;

import com.beans.Address;
import com.beans.Order;
import com.beans.Pizza;
import com.beans.Product;
import com.beans.User;
import com.beans.UserProducts;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class OrderDB {
    
    public static ArrayList<Order> getOrders() throws SQLException, ClassNotFoundException {
        String s = "SELECT USERS.USERID,USERS.NAME,USERS.SURNAME,USERS.PHONE,USERS.EMAIL,"
                + "ORDER_PRODUCTID,ORDER_PRODUCT.PRODUCTID,PRODUCTS.NAME,CATEGORY,COST,ORDER_PRODUCT.ORDERSID,QUANTITY,DELIVERYDATE,RECEIVED,SHIPPED,ORDERS.ADDRESSID,ADDRESSNAME "
                + "FROM ORDER_PRODUCT,ADDRESS,ORDERS,PRODUCTS,USERS "
                + "WHERE PRODUCTS.PRODUCTID=ORDER_PRODUCT.PRODUCTID"
                + " AND ORDERS.ADDRESSID=ADDRESS.ADDRESSID "
                + "AND ORDERS.ORDERSID=ORDER_PRODUCT.ORDERSID "
                + "AND USERS.USERID=ORDERS.USERID";
        ArrayList<Order> orders = new ArrayList<Order>();

        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ResultSet rs = ps.executeQuery();
            int oldOrderID = 0;
            Order order = null;
            while (rs.next()) {
                int orderID = rs.getInt("ORDERSID");
                if (oldOrderID == 0 || oldOrderID != orderID) {
                    Address address = new Address(rs.getInt("ADDRESSID"), rs.getString("ADDRESSNAME"));
                    order = new Order(orderID, rs.getTimestamp("DELIVERYDATE"), rs.getTimestamp("SHIPPED"), address, new ArrayList<UserProducts>());
                    oldOrderID = orderID;
                    Product product = new Product(rs.getInt("PRODUCTID"), rs.getString("NAME"), rs.getDouble("COST"), rs.getString("CATEGORY"));
                    UserProducts product_quantity = new UserProducts(rs.getInt("ORDER_PRODUCTID"), product, rs.getInt("QUANTITY"));
                    order.getProducts().add(product_quantity);
                    order.setReceived(rs.getBoolean("RECEIVED"));
                    User user = new User(rs.getInt("USERS.USERID"),rs.getString("USERS.NAME"),rs.getString("USERS.SURNAME"),rs.getString("USERS.EMAIL"),rs.getString("USERS.PHONE"));
                    order.setUser(user);
                    orders.add(order);
                } else {
                    Product product = new Product(rs.getInt("PRODUCTID"), rs.getString("NAME"), rs.getDouble("COST"), rs.getString("CATEGORY"));
                    UserProducts product_quantity = new UserProducts(rs.getInt("ORDER_PRODUCTID"), product, rs.getInt("QUANTITY"));
                    order.getProducts().add(product_quantity);
                }
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }

        return orders;
    }

}
