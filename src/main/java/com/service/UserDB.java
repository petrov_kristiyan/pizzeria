/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.service;

import com.beans.Address;
import com.beans.Cart;
import com.beans.Order;
import com.beans.UserProducts;
import com.beans.Pizza;
import com.beans.Product;
import com.beans.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.sql.rowset.CachedRowSet;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * @author kristiyan
 */
public class UserDB {

    public static User login(String email, String password) throws SQLException, ClassNotFoundException {
        User user = new User();
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String s = "SELECT * FROM USERS LEFT JOIN ADDRESS ON USERS.ADDRESSID=ADDRESS.ADDRESSID WHERE EMAIL=?";
        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (passwordEncoder.matches(password, rs.getString("PASSWORD"))) {
                    user.setUserid(rs.getInt("USERID"));
                    user.setEmail(email);
                    user.setPhone(rs.getString("PHONE"));
                    user.setName(rs.getString("NAME"));
                    user.setSurname(rs.getString("SURNAME"));
                    user.setRole(rs.getString("ROLE"));
                    user.setPassword(rs.getString("PASSWORD"));
                    user.setAddress(new Address(rs.getInt("ADDRESSID"), rs.getString("ADDRESSNAME")));
                }

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }

        if (user.getUserid() != 0) {

            s = ("SELECT * FROM CART,PRODUCTS WHERE USERID=? AND CART.PRODUCTID=PRODUCTS.PRODUCTID");
            ps = DBManager.createStatement(s);
            try {
                ps.setInt(1, user.getUserid());

                ResultSet rs = ps.executeQuery();
                Cart cart = new Cart();
                LinkedHashMap<Integer, UserProducts> cartOrders = new LinkedHashMap<>();
                while (rs.next()) {
                    Product product;

                    if (rs.getString("CATEGORY").equals("classic")) {
                        product = new Pizza();
                        ((Pizza) product).setDescription(rs.getString("DESCRIPTION"));
                        product.setCost(rs.getDouble("COST"));
                        String ingredients = rs.getString("INGREDIENTS");
                        if (ingredients != null) {
                            ArrayList<String> ingredientsList = new ArrayList<String>();
                            Collections.addAll(ingredientsList, ingredients.split(","));
                            ((Pizza) product).setIngredients(ingredientsList);
                        }
                        product.setId(rs.getInt("PRODUCTID"));
                        product.setName(rs.getString("NAME"));
                        product.setCategory(rs.getString("CATEGORY"));
                        product.setCost(rs.getDouble("COST"));
                        cartOrders.put(product.getId(), new UserProducts(rs.getInt("CARTID"), product, rs.getInt("QUANTITY")));
                    }
                }
                cart.setOrderList(cartOrders);
                user.setCart(cart);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                Connection con = ps.getConnection();
                ps.close();
                con.close();
            }
        }
        user.setHistory(loadUserOrders(user));
        return user;
    }

    public static User register(String email, String password) throws SQLException, ClassNotFoundException {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        password = passwordEncoder.encode(password);
        User user = new User();
        String s = "INSERT INTO USERS(EMAIL,PASSWORD) VALUES(?,?)";
        PreparedStatement ps = DBManager.createStatement(s);
        user.setEmail(email);
        try {
            ps.setString(1, email);
            ps.setString(2, password);
            ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    user.setUserid(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("problem ID");
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
        return user;
    }

    public static ArrayList<Order> loadUserOrders(User user) throws SQLException, ClassNotFoundException {
        String s = "SELECT ORDER_PRODUCTID,ORDER_PRODUCT.PRODUCTID,NAME,CATEGORY,COST,ORDER_PRODUCT.ORDERSID,QUANTITY,DELIVERYDATE,RECEIVED,SHIPPED,ORDERS.ADDRESSID,ADDRESSNAME "
                + "FROM ORDER_PRODUCT,ADDRESS,ORDERS,PRODUCTS "
                + "WHERE PRODUCTS.PRODUCTID=ORDER_PRODUCT.PRODUCTID AND ORDERS.ADDRESSID=ADDRESS.ADDRESSID AND ORDERS.ORDERSID=ORDER_PRODUCT.ORDERSID AND ORDER_PRODUCT.USERID=?";
        ArrayList<Order> history = new ArrayList<Order>();

        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ps.setInt(1, user.getUserid());

            ResultSet rs = ps.executeQuery();
            int oldOrderID = 0;
            Order order = null;
            while (rs.next()) {
                int orderID = rs.getInt("ORDERSID");
                if (oldOrderID == 0 || oldOrderID != orderID) {
                    Address address = new Address(rs.getInt("ADDRESSID"), rs.getString("ADDRESSNAME"));
                    order = new Order(orderID, rs.getTimestamp("DELIVERYDATE"), rs.getTimestamp("SHIPPED"), address, new ArrayList<UserProducts>());
                    oldOrderID = orderID;
                    Product product = new Product(rs.getInt("PRODUCTID"), rs.getString("NAME"), rs.getDouble("COST"), rs.getString("CATEGORY"));
                    UserProducts product_quantity = new UserProducts(rs.getInt("ORDER_PRODUCTID"), product, rs.getInt("QUANTITY"));
                    order.getProducts().add(product_quantity);
                    order.setReceived(rs.getBoolean("RECEIVED"));
                    history.add(order);
                } else {
                    Product product = new Product(rs.getInt("PRODUCTID"), rs.getString("NAME"), rs.getDouble("COST"), rs.getString("CATEGORY"));
                    UserProducts product_quantity = new UserProducts(rs.getInt("ORDER_PRODUCTID"), product, rs.getInt("QUANTITY"));
                    order.getProducts().add(product_quantity);
                }
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }

        return history;
    }

    public static boolean editProfile(User user, String name, String surname, String addressName, String phone, String email, String password) throws SQLException, ClassNotFoundException {
        Address address = new Address();
        address.setId(retriveAddressId(user, addressName));
        String s = "UPDATE USERS SET NAME=?,SURNAME=?,ADDRESSID=?,PHONE=?,EMAIL=?,PASSWORD = ? WHERE USERID = ?";
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (password != null && password.length() >= 6) {
            password = passwordEncoder.encode(password);
        }
        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ps.setString(1, name == null || name.length() == 0 ? user.getName() : name);
            ps.setString(2, surname == null || surname.length() == 0 ? user.getSurname() : surname);
            ps.setInt(3, address.getId());
            ps.setString(4, phone == null || phone.length() == 0 ? user.getPhone() : phone);
            ps.setString(5, email == null || email.length() == 0 ? user.getEmail() : email);
            ps.setString(6, password == null || password.length() < 6 ? user.getPassword() : password);
            ps.setInt(7, user.getUserid());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }

        if (addressName.length() > 0) {
            address.setAddress(addressName);
            user.setAddress(address);
        }
        if (name.length() > 0) {
            user.setName(name);
        }
        if (surname.length() > 0) {
            user.setSurname(surname);
        }
        if (phone.length() > 3) {
            user.setPhone(phone);
        }
        if (email != null && email.length() > 0) {
            user.setEmail(email);
        }
        if (password != null && password.length() >= 6) {
            user.setPassword(password);
        }
        return true;
    }

    public static boolean saveOrder(User user, String addressName, Date deliveryDate) throws SQLException, ClassNotFoundException {
        Order order = new Order();
        order.setDeliveryDate(new Timestamp(deliveryDate.getTime()));
        Address address = new Address();
        address.setAddress(addressName);
        address.setId(retriveAddressId(user, addressName));
        String s = "INSERT INTO ORDERS (ADDRESSID,DELIVERYDATE,USERID) VALUES(?,?,?)";
        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ps.setInt(1, address.getId());
            ps.setTimestamp(2, order.getDeliveryDate());
            ps.setInt(3, user.getUserid());
            ps.executeUpdate();
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    order.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("problem ID");
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
        for (Map.Entry<Integer, UserProducts> cartProducts : user.getCart().getOrderList().entrySet()) {
            s = "INSERT INTO ORDER_PRODUCT (PRODUCTID,ORDERSID,USERID,QUANTITY) VALUES(?,?,?,?)";
            UserProducts product = cartProducts.getValue();
            ps = DBManager.createStatement(s);
            try {
                ps.setInt(1, product.getProduct().getId());
                ps.setInt(2, order.getId());
                ps.setInt(3, user.getUserid());
                ps.setInt(4, product.getQuantity());
                ps.executeUpdate();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return false;
            } finally {
                Connection con = ps.getConnection();
                ps.close();
                con.close();
            }
        }
        order.setAddress(address);
        order.setProducts(new ArrayList<>(user.getCart().getOrderList().values()));
        user.getHistory().add(order);
        user.getCart().setOrderList(new LinkedHashMap<Integer, UserProducts>());
        s = "DELETE FROM CART WHERE USERID=?";
        ps = DBManager.createStatement(s);

        try {
            ps.setInt(1, user.getUserid());
           return ps.executeUpdate()!=1;
            

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
        return true;
    }

    /**
     * Get Address id if it exists, else create the address and return the id
     *
     * @param addressName
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static int retriveAddressId(User user, String addressName) throws SQLException, ClassNotFoundException {
        if (user.getAddress().getAddress() != null && user.getAddress().getId() != 0 && user.getAddress().equals(addressName)) {
            return user.getAddress().getId();
        }
        String s = "SELECT ADDRESSID FROM ADDRESS WHERE ADDRESSNAME=?";
        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ps.setString(1, addressName);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("ADDRESSID");
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
        if (addressName != null && !addressName.isEmpty()) {
            s = "INSERT INTO ADDRESS (ADDRESSNAME) VALUES(?)";
            ps = DBManager.createStatement(s);
            try {
                ps.setString(1, addressName);
                ps.executeUpdate();
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    } else {
                        throw new SQLException("problem ID");
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            } finally {
                Connection con = ps.getConnection();
                ps.close();
                con.close();
            }
        }
        return 0;
    }

    public static boolean deleteOrder(User user, int id) throws SQLException, ClassNotFoundException {
        String s = "DELETE FROM ORDERS WHERE ORDERSID=? AND RECEIVED=0 AND USERID=?";

        int i = 0;
        for (; i < user.getHistory().size(); i++) {
            if (user.getHistory().get(i).getId() == id) {
                break;
            }
        }
        if (i >= 0 && i < user.getHistory().size()) {
            user.getHistory().remove(i);
        }

        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ps.setInt(1, id);
            ps.setInt(2, user.getUserid());
            ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }

        return true;
    }

    public static boolean confirmDelivery(User user, int id) throws ClassNotFoundException, SQLException {
        String s = "UPDATE ORDERS SET RECEIVED=? WHERE USERID = ? AND ORDERSID=?";
        PreparedStatement ps = DBManager.createStatement(s);
        try {
            ps.setBoolean(1, true);
            ps.setInt(2, user.getUserid());
            ps.setInt(3, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        } finally {
            Connection con = ps.getConnection();
            ps.close();
            con.close();
        }
        for (Order order : user.getHistory()) {
            if (order.getId() == id) {
                order.setReceived(true);
                break;
            }
        }
        return true;
    }
}
