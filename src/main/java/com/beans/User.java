package com.beans;

import com.service.UserDB;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@Component
@Scope("session")
public class User implements Serializable{
    private int userid;
    private String password;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String role;
    private Address address;
    private List<Order> history;
    private Cart cart;

    public User(){
        userid=0;
    }

    public User(int userid, String name, String surname, String email, String phone) {
        this.userid = userid;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public static User login(String email,String password) throws SQLException, ClassNotFoundException{
        return UserDB.login(email,password);
    }
    
    public  static User register(String email,String password) throws SQLException, ClassNotFoundException{
        return UserDB.register(email,password);
    }
    
    

    public List<Order> getHistory() {
        return history;
    }

    public void setHistory(List<Order> history) {
        this.history = history;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart){
        this.cart = cart;
    }
    
    public void clone(User user) {
        this.userid = user.userid;
        this.password = user.password;
        this.name = user.name;
        this.surname = user.surname;
        this.email = user.email;
        this.phone = user.phone;
        this.role = user.role;
        this.address = user.address;
        this.history = user.history;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
    public boolean editProfile(User user,String name,String surname,String address,String phone,String email,String password) throws SQLException, ClassNotFoundException{
        return UserDB.editProfile(user,name,surname,address,phone,email,password);
    }
        
    public boolean saveOrder(User user,String addressName,Date deliveryDate) throws SQLException, ClassNotFoundException{
        return UserDB.saveOrder(user, addressName,deliveryDate);
    }
    public boolean deleteOrder(User user,int id) throws SQLException, ClassNotFoundException{
        return UserDB.deleteOrder(user,id);
    }
    public boolean confirmDelivery(User user,int id) throws SQLException, ClassNotFoundException{
        return UserDB.confirmDelivery(user,id);
    }
}
