/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beans;

import com.service.CartDB;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class Cart implements Serializable {
    
    private LinkedHashMap<Integer,UserProducts> orderList;
    private boolean changed;

    public Cart() {
        orderList = new LinkedHashMap<>();
    }

    public Cart(LinkedHashMap<Integer,UserProducts> orderList) {
        this.orderList = orderList;
    }

    public LinkedHashMap<Integer,UserProducts> getOrderList() {
        return orderList;
    }

    public void setOrderList(LinkedHashMap<Integer,UserProducts> orderList) {
        this.orderList = orderList;
    }

    public boolean addOrder(Product product,int newQuantity) {
        setChanged(true);
        for (Map.Entry<Integer, UserProducts> entry : orderList.entrySet()) {
            if( entry.getKey() == product.getId() ){
                int quantity = entry.getValue().getQuantity() + newQuantity;
                entry.getValue().setQuantity(quantity);  
            }
        }
        UserProducts products= new UserProducts();
        products.setQuantity(newQuantity);
        products.setProduct(product);
        orderList.put(product.getId(),products);
        return true;
    }

    public void updateOrder(int productId,int quantity) {
        for (Map.Entry<Integer, UserProducts> entry : orderList.entrySet()) {
            if( entry.getKey() == productId ){
                setChanged(true);
                entry.getValue().setQuantity(quantity);                
            }
        }        
    }

    public boolean removeOrderById(int id) {
        setChanged(true);
        
        return orderList.remove(id)!=null;
    }
    /**
     * Merge of dbCart and sessionCart, keeping the session cart values and adding the missing products
     * @param sessionCart 
     * @param dbCart
     * @return 
     */
    public static LinkedHashMap<Integer,UserProducts> joinCarts(Cart sessionCart, Cart dbCart) {
        LinkedHashMap<Integer,UserProducts> orderListSession = sessionCart.getOrderList();
        LinkedHashMap<Integer,UserProducts> orderListDB = dbCart.getOrderList();
        orderListDB.putAll(orderListSession);
        return orderListDB;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }
    
    public static void saveCart(User user) throws SQLException, ClassNotFoundException{
        CartDB.saveCart(user);
    }
    
}
