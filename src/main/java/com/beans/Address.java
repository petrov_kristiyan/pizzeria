/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beans;

import java.io.Serializable;

/**
 *
 * @author kristiyan
 */
public class Address implements Serializable {
    private int id;
    private String address;

    public Address(int adressid, String address) {
        this.id = adressid;
        this.address = address;
    }

    public Address() {
        id=0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }    
    
}
