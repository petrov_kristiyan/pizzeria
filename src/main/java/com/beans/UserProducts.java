package com.beans;


/**
 *
 * @author kristiyan
 */
public class UserProducts {
    private int id;
    private Product product;
    private int quantity;
        
    public UserProducts(){
        id=0;
    }
    
    public UserProducts(int id,Product product,int quantity) {
        this.id=id;
        this.product=product;
        this.quantity=quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 999 && quantity > 0) {
            this.quantity = quantity;
        }
    }

  
}
