/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beans;

import com.service.OrderDB;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author kristiyan
 */
public class Order {
    private ArrayList<UserProducts> products;
    private int id;
    private Timestamp deliveryDate;
    private Timestamp shipped;
    private Address address;
    private boolean received;
    private User user;
  
    public Order(){
        id=0;
    }
    
    
    public Order(int id, Timestamp deliveryDate, Timestamp shipped, Address address, ArrayList<UserProducts> products) {
        this.products = products;
        this.id=id;
        this.deliveryDate = deliveryDate;
        this.shipped = shipped;
        this.address = address;
    }

    public Timestamp getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Timestamp deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Timestamp getShipped() {
        return shipped;
    }

    public void setShipped(Timestamp shipped) {
        this.shipped = shipped;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
  
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public ArrayList<UserProducts> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<UserProducts> products) {
        this.products = products;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }
    
    public static ArrayList<Order> getAllOrders() throws SQLException, ClassNotFoundException{
        return OrderDB.getOrders();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
     
}
