package com.beans;

import com.service.ProductDB;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.context.annotation.Scope;
import org.springframework.web.multipart.MultipartFile;

@Scope("session")
public class Product {

    private int id;
    private String name;
    private double cost;
    private String category;
    private String description;

    public Product() {
        name = "";
        cost = 0;
        category = "classic";
        description = "";
    }

    public Product(int id, String name, double cost, String category) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<Product> getProducts() throws SQLException, ClassNotFoundException {
        return ProductDB.getProducts(category);
    }

    public static boolean deleteProduct(int id) throws ClassNotFoundException, SQLException {
        return ProductDB.deleteProduct(id);
    }

    public static Product insertProduct(MultipartFile file,String category,String description, String name, String ingredients, Double price, String path) throws ClassNotFoundException, SQLException {
        return ProductDB.insertProduct(file,category,description, name, ingredients, price, path);
    }
    public static boolean updateProduct(MultipartFile file,String category,String description, String name, String ingredients, Double price, String path,int id) throws ClassNotFoundException, SQLException {
        return ProductDB.updateProduct(file,category,description, name, ingredients, price, path,id);
    }
}
