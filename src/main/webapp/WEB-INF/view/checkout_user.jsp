<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row" id='realBody'>
    <span id="titles">
        <div class="secondaryTitle title">Cart revision</div>
        <div class="mainTitle title">User details</div>
        <div class="secondaryTitle title">Delivery details</div>
        <div class="thirdTitle title">Complete order</div>
    </span>
    <div class="col-md-9 main well" style="z-index: 1;">
        <p id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Please before proceeding to the next phase complete your profile</p>
        <form class="well form-horizontal" id="form-editProfile" method="POST" action="">
            <div class="form-group">  
                <label for="inputName" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" value="${user.name}" id="inputName" class=" form-control" placeholder="Name" required pattern="[a-zA-Z]{3,}" title="Minimum 3 letters.">
                </div>
            </div>
            <div class="form-group">
                <label for="inputSurname" class="col-sm-2 control-label">Surname</label>
                <div class="col-sm-10">
                    <input type="text" value="${user.surname}" name="surname" id="inputSurname" class=" form-control " placeholder="Surname" required pattern="[a-zA-Z]{3,}" title="Minimum 3 letters.">
                </div>
            </div>
            <div class="form-group">
                <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <input type="text" name="address" value="${user.address.address}"id="inputAddress" class=" form-control" placeholder="Address" required>
                </div>
            </div>
            <div class="form-group has-feedback">
                <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                    <input type="tel" name="phone" value="${user.phone}" id="inputPhone" class=" form-control bfh-phone" placeholder="Phone number" required>
                    <span class=" hide glyphicon form-control-feedback" id="inputPhoneValidation"></span>
                    <script>

                        $("#inputPhone").intlTelInput({
                            autoFormat: true,
                            autoHideDialCode: false,
                            defaultCountry: "auto",
                            utilsScript: "http://www.techieat.com/international-telephone/lib/libphonenumber/build/utils.js"
                        });
                        var telInput = $("#inputPhone");
                        telInput.blur(function () {
                            if ($.trim(telInput.val())) {
                                if (!telInput.intlTelInput("isValidNumber")) {
                                    telInput.closest("div[class^='form-group']").removeClass("has-success").addClass("has-error");
                                    $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-ok").addClass("glyphicon-remove");
                                }
                                else {
                                    telInput.closest("div[class^='form-group']").removeClass("has-error").addClass("has-success");
                                    $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-remove").addClass("glyphicon-ok");
                                }
                            }
                        });
                        telInput.keyup(function () {
                            if (!telInput.intlTelInput("isValidNumber")) {
                                telInput.closest("div[class^='form-group']").removeClass("has-success").addClass("has-error");
                                $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-ok").addClass("glyphicon-remove");
                            }
                            else {
                                telInput.closest("div[class^='form-group']").removeClass("has-error").addClass("has-success");
                                $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-remove").addClass("glyphicon-ok");
                            }
                        });
                            $("#form-editProfile").on('submit',function (e)
                            {
                                 e.preventDefault();
                                var postData = $(this).serializeArray();
                                $.ajax(
                                        {
                                            url: "editProfile.html",
                                            type: "POST",
                                            data: postData,
                                            dataType: "json",
                                            success: function (response) {
                                               if(response.status=="ok"){
                                               nextPhaseCheckout();
                                            }
                                            },
                                            error: function ()
                                            {
                                                $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Error while submitting this data </div>').insertBefore("#form-editProfile");

                                            }
                                        });
                                //STOP default action
                            });
                    </script>
                </div>
            </div>
            <span class="col-md-12 col-xs-12" style='padding:0;'> 
                <a  id='buttonCancel' class='btn btn-default span7 text-center col-md-3 col-xs-3' href="/">Cancel</a>
                <button type='submit' id='buttonProceed' class='btn btn-success span7 text-center col-md-3 col-md-offset-6 col-xs-5 col-xs-offset-4'>Save & Proceed</button>
            </span>
        </form>
    </div>
</div>


