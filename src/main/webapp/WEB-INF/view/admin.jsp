<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <title>Pizzeria</title>
        <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/img/favicon.ico"/>" />


        <script src="<c:url value="/resources/script/jquery-2.1.3.min.js"/>"></script>  
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"/>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"/>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/dropzone.css"/>"/>

        <script src="<c:url value="/resources/script/dropzone.js"/>"></script>

        <script src="<c:url value="/resources/script/adminScript.js"/>"></script>

    </head>
    <body>  
        <div class="container-fluid">
            <header class="navbar navbar-default">
                <div class="">
                    <div class="logo-header">
                        <span class="">

                            <a class="navbar-brand" href="index">Pizzeria</a>

                            <a class="navbar-brand" href="/index">User Section</a>

                            <a class="navbar-brand" href="orders">All Orders</a>
                        </span>
                        
                            <div class="pull-right adminProfile" id="profile" >
                                <button id="profileButton"  style="background-image:url('<c:url value="/resources/img/user.png"/>');" ></button>
                                <label style="vertical-align: bottom;">${email}</label>
                                <ul class="list-group" id="profileMenu">
                                    <li class="list-group-item btn btn-danger"><a href="logout.html" >Logout</a></li>
                                </ul>                                  
                            </div>
                        
                    </div>
                </div>
            </header> 
            <div class="row" id='realBody'>
                <div class="col-md-9 col-md-offset-2 main" style="z-index: 1">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#">Classic Recipes</a></li>
                    </ul>
                    <div data-columns="" id="columns" class="row row-flex row-flex-wrap">
                        <div class="col-md-4 col-sm-5 col-xs-12">
                            <div class="panel panel-default flex-col">
                                <div class="panel-body flex-grow">
                                    <img class='img-thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12 '  data-toggle="modal" data-target="#insertModal" src='<c:url value='/resources/img/add.png'/>' onclick="showModal();">
                                </div> 
                            </div>
                        </div>
                        <c:if test="${not empty list}">
                            <c:forEach var="product" items="${list}">
                                <div class="col-md-4 col-sm-5 col-xs-12" id="product${product.id}">
                                    <div class="panel panel-default flex-col">
                                        <strong class="panel-heading">
                                            ${product.name}  
                                            <img class='img-delete pull-right' src='<c:url value='/resources/img/delete.png'/>' onclick="deleteProduct(${product.id})">
                                            <img class='img-edit pull-left' src='<c:url value='/resources/img/edit.png'/>' onclick="showEditProduct(${product.id})">
                                        </strong>

                                        <div class="panel-body flex-grow">
                                            <div class="fix">
                                                <img class='img-thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12 ' src='<c:url value='/resources/img/products/${product.id}.jpg?time=${time}'/>'>
                                                <div class="desc">
                                                    <p class="desc_content"><em>${product.ingredients}</em></p>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="panel-footer clearfix"> 
                                            <div class='row vertical-align'>
                                                <div class="col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3 col-md-6 col-md-offset-3">
                                                    <div class='cost'>                                               
                                                        <fmt:formatNumber value="${product.cost}"  type="currency"/>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-xs-3 col-md-3">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                </div>
                <div class="col-md-2 sidebar" style="bottom: 40px; z-index: 0;">
                    <img id="logo" src="<c:url value="/resources/img/pizzeria.jpg"/>"  alt="Pizzeria" width="162" height="136">
                </div>
                <div id="showR"></div>
                <jsp:include page="info.jsp"></jsp:include>
                    <div id="push"></div>
                </div>
            </div>
        <jsp:include page="footer.jsp" />