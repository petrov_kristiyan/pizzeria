<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <title>Pizzeria</title>

        <script src="<c:url value="/resources/script/jquery-2.1.3.min.js"/>"></script>  
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"/>

        <script src="<c:url value="/resources/script/script.js"/>"></script>

    </head>
    <body> 

        <div class="container-fluid">
            <header class="navbar navbar-default">
                <div class="">
                    <div class="logo-header">
                        <a class="navbar-brand col-xs-1" href="index">Pizzeria</a>

                    </div>
            </header> 
            <div class="row" id='realBody'>
                <div class="col-md-9 col-md-offset-2 main" style="z-index: 1">
                    <div class="modal-header">
                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                            <li id="loginLink" class="active"><a href="#login"  data-toggle="tab"><h4 >Login</h4></a></li>
                            <li id="registerLink"><a href="#register" data-toggle="tab"><h4 >Register</h4></a></li>
                        </ul>
                    </div>
                    <div id="my-tab-content" class="tab-content">
                        <div class="tab-pane active" id="login">
                            <div class="modal-body" >
                                <%
                                    if (request.getParameter("error") != null) {
                                        out.println("<div id =\"error\" class=\"alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a><strong>Error!</strong> Failed to login using this email and password.</div>");
                                    }
                                %>
                                <form class="well form-horizontal" action="<c:url value='j_spring_security_check' />"   method="POST" role="form"  >
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="j_username" id="inputEmail" class="form-control" placeholder="Email address" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="j_password" id="inputPassword" class="form-control" placeholder="Password" required>
                                        </div>
                                    </div>
                                    <input type="submit" id="loginSubmit" value="submit" style="display:none;">	
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button class="btn btn-success btn-md col-md-offset-7" type="submit" onclick="$('#loginSubmit').click();">Login</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="register">
                            <div class="modal-body">
                                <form class="well form-horizontal" autocomplete="off" id="form-register" role="form" method="POST"  >
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password1" id="inputPassword" class="form-control" placeholder="Password" required pattern="[a-zA-Z0-9]{6,}" title="Minimum 6 letters or numbers.">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmPassword" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password2" id="confirmPassword" class=" form-control" placeholder="Confirm Password" required pattern="[a-zA-Z0-9]{6,}" title="Minimum 6 letters or numbers." >
                                        </div>
                                    </div>
                                    <input type="submit" id="registerSubmit" value="submit" style="display:none;">			
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button class="btn btn-primary btn-md col-md-offset-7" type="submit" onclick="registerSubmit();">Register</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 sidebar" style="bottom: 40px; z-index: 0;">
                    <img id="logo" src="<c:url value="/resources/img/pizzeria.jpg"/>"  alt="Pizzeria" width="162" height="136">
                </div>
                <div id="showR"></div>
                <jsp:include page="info.jsp"></jsp:include>
                    <div id="push"></div>

                </div>
            </div>
        <jsp:include page="footer.jsp" />