<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <title>Pizzeria</title>
        <link rel="shortcut icon" type="image/x-icon" href="<c:url value="/resources/img/favicon.ico"/>" />

        <script src="<c:url value="/resources/script/jquery-2.1.3.min.js"/>"></script>  
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"/>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="http://www.techieat.com/international-telephone/build/css/intlTelInput.css"/>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"/>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/jquery.bootstrap-touchspin.css"/>"/>

        <script src="<c:url value="/resources/script/script.js"/>"></script>

        <script src="<c:url value="/resources/script/jquery.bootstrap-touchspin.js"/>"></script>

    </head>
    <body>  
        <div class="container-fluid">
            <header class="navbar navbar-default">
                <div class="">
                    <div class="logo-header">
                        <a class="navbar-brand col-xs-1 " href="index">Pizzeria</a>
                        <c:if test="${role eq 'ROLE_ADMIN' }">
                            <a class="navbar-brand col-xs-5 " href="/admin/index">Admin Section</a>
                        </c:if>
                        <div id="navbar" class="pull-right"> 
                            <c:choose>
                                <c:when test="${empty email}">
                                    <button type="button" id="loginButton" class="btn  btn-success">Login</button>
                                    <label class="text-muted">OR</label>
                                    <button class="btn  btn-primary" id="registerButton" type="button" > Register</button>
                                </c:when>
                                <c:otherwise>
                                    <div class="pull-left" id="profile">
                                        <button id="profileButton"  style="background-image:url('<c:url value="/resources/img/user.png"/>');" ></button>
                                        <label style="vertical-align: bottom;">${email}</label>
                                        <ul class="list-group" id="profileMenu">
                                            <li class="list-group-item"><a href="user/profile.html">Edit Profile</a></li>
                                            <li class="list-group-item"><a href="user/orders.html">Orders</a></li>
                                            <li class="list-group-item btn btn-danger"><a href="user/logout.html" >Logout</a></li>
                                        </ul>                                  
                                    </div>
                                </c:otherwise>
                            </c:choose>
                            <div class="pull-right" id="cart">
                                <div class="" style="text-align: center; padding-bottom: 5px;" >
                                    <button class="" id="cartButton"  style="background-image:url('<c:url value="/resources/img/cartFull.png"/>');" >
                                        <span class="badge badge-danger btn-danger" id="cartProductsNumber">
                                            <c:set var='totalNumber' value="${0}"/>
                                            <script>
                                                if (${totalNumber} >= 1) {
                                                    $("#cartProductsNumber").show();
                                                    $("#cartProductsNumber").html(${totalNumber});
                                                }
                                                else {
                                                    $("#cartProductsNumber").hide();
                                                }
                                                $('header').affix({
                                                    offset: {
                                                        top: $('header').height()

                                                    }
                                                });
                                            </script>
                                        </span>
                                    </button>
                                    <form id="cartContent" class="navbar-default cartContent"  method="GET" autocomplete="off" action="checkout.html">
                                        <c:choose>
                                            <c:when test="${not empty cart.orderList}">
                                                <table id='cartBody'>
                                                    <c:forEach var="order" items="${cart.orderList}"> 
                                                        <c:set var='sum' value="${0+sum+order.value.quantity*order.value.product.cost}"/>
                                                        <c:set var='totalNumber' value="${totalNumber+order.value.quantity}"/>
                                                        <tr class='rowCart'>
                                                            <td>
                                                                <img class='img-thumbnail img-previewProduct' src='<c:url value='/resources/img/products/${order.value.product.id}.jpg'/>'>
                                                            </td>
                                                            <td>${order.value.product.name} </td>
                                                            <td>
                                                                <input name="inputQuantity" class="inputQuantity" id="inputQuantity${order.value.product.id}" type="text" value="${order.value.quantity}" onkeyup="inputQuantityCheck(this);
                                                                        addOrder(${order.value.product.id}, $(this).val());" onchange="inputQuantityCheck(this);
                                                                                addOrder(${order.value.product.id}, $(this).val());">
                                                                <script>
                                                                    $("#inputQuantity${order.value.product.id}").TouchSpin({
                                                                        verticalbuttons: true,
                                                                        min: 1,
                                                                        max: 999
                                                                    });
                                                                </script>
                                                            </td>
                                                            <td class="productCartCost">
                                                                <fmt:formatNumber value="${order.value.product.cost}" type="currency"/>
                                                            </td>
                                                            <td >
                                                                <img class='img-delete' src='<c:url value='/resources/img/delete.png'/>' onclick="deleteOrder(${order.value.product.id})">
                                                            </td>


                                                        </tr>
                                                    </c:forEach>
                                                    <script>
                                                        if (${totalNumber} >= 1) {
                                                            $("#cartProductsNumber").show();
                                                            $("#cartProductsNumber").html(${totalNumber});
                                                        }
                                                        else {
                                                            $("#cartProductsNumber").hide();
                                                        }
                                                    </script>
                                                </table>
                                                <div id="cartFooter">
                                                    <hr style="width: 100%; color: white; height: 1px; background-color:white; margin:5px;" />
                                                    <span>
                                                        <strong class="pull-right">
                                                            Total: <fmt:formatNumber value="${sum}" type="currency"/>                              
                                                        </strong>
                                                    </span>
                                                    <span class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style='text-align: center;padding: 15px 0;'> 
                                                        <button type='submit' id='buttonCheckout' class='btn btn-danger span7 text-center' >Checkout</button>
                                                    </span>
                                                </div>
                                            </c:when>
                                            <c:otherwise> 
                                                <div id="cartBody">
                                                    <c:set var="sum" value="0"></c:set>
                                                    <img class='img-cart' src='<c:url value='/resources/img/cartFull.png'/>'>
                                                    <em>Your cart feels empty, sad and alone :( </em>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                        <div class="triangle"></div>
                                    </form>
                                    <label id='cartSum'>
                                        <fmt:formatNumber value="${sum}" type="currency"/>
                                    </label> 
                                </div>
                            </div>
                        </div>
                    </div>
            </header> 
            <div class="row" id='realBody'>
                <div class="col-md-9 col-md-offset-2 main" style="z-index: 1">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#">Classic Recipes</a></li>
                    </ul>
                    <c:if test="${not empty list}">
                        <div data-columns="" id="columns" class="row row-flex row-flex-wrap">
                            <c:forEach var="product" items="${list}">
                                <div class="col-md-4 col-sm-5 col-xs-12">
                                    <div class="panel panel-default flex-col">
                                        <strong class="panel-heading">${product.name}</strong>
                                        <div class="panel-body flex-grow">
                                            <div class="fix">
                                                <img class='img-thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12 ' src='<c:url value='/resources/img/products/${product.id}.jpg'/>'>
                                                <div class="desc">
                                                    <p class="desc_content"><em>${product.ingredients}</em></p>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="panel-footer clearfix"> 
                                            <div class='row vertical-align'>
                                                <div class="col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3 col-md-6 col-md-offset-3">
                                                    <div class='cost'>                                               
                                                        <fmt:formatNumber value="${product.cost}"  type="currency"/>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-xs-3 col-md-3">
                                                    <input type='image' data-id='${product.id}' class="img-responsive addCart col-lg-12 col-md-12 col-sm-12 col-xs-12 " data-toggle="modal" data-target="#quantityFormModal" src='<c:url value='/resources/img/addCart.png'/>' onclick="showModal('${product.id}');"/> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </c:forEach>
                        </div>
                    </c:if>
                </div>
                <div class="col-md-2 sidebar" style="bottom: 40px; z-index: 0;">
                    <img id="logo" src="<c:url value="/resources/img/pizzeria.jpg"/>"  alt="Pizzeria" width="162" height="136">
                </div>
                <div id="showR"></div>
                <jsp:include page="info.jsp"></jsp:include>
                    <div id="push"></div>
                </div>
            </div>
        <jsp:include page="footer.jsp" />