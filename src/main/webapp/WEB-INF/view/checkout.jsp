<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <title>Pizzeria</title>

        <script src="<c:url value="/resources/script/jquery-2.1.3.min.js"/>"></script>  
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"/>
        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/jquery.datetimepicker.css"/>"/>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/jquery.bootstrap-touchspin.css"/>"/>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="<c:url value="/resources/script/script.js"/>"></script>
        <link rel="stylesheet" href="http://www.techieat.com/international-telephone/build/css/intlTelInput.css">

        <script src="http://www.techieat.com/international-telephone/build/js/intlTelInput.js"></script>

        <script src="<c:url value="/resources/script/jquery.bootstrap-touchspin.js"/>"></script>
        <script src="<c:url value="/resources/script/jquery.datetimepicker.js"/>"></script>

    </head>
    <body>  
        <div class="container-fluid">
            <header class="navbar navbar-default">
                <div class="">
                    <div class="logo-header">
                        <a class="navbar-brand col-xs-1" href="index">Pizzeria</a>

                    </div>
            </header> 
            <div id="container" class="col-md-offset-3 col-md-9">
                <div class="row" id='realBody'>
                    <span id="titles">
                        <div class="mainTitle title">Cart revision</div>
                        <div class="secondaryTitle title">Delivery details</div>
                        <div class="thirdTitle title">Complete order</div>
                    </span>
                    <div class="col-md-9 main well" style="z-index: 1;">
                        <div id="cartContent">
                            <c:choose>
                                <c:when test="${not empty cart.orderList}">
                                    <table id='cartBodyCheckout'>
                                        <c:forEach var="order" items="${cart.orderList}"> 
                                            <c:set var='sum' value="${0+sum+order.value.quantity*order.value.product.cost}"/>
                                            <c:set var='totalNumber' value="${totalNumber+order.value.quantity}"/>
                                            <tr class='rowCart'>
                                                <td>
                                                    <img class='img-thumbnail img-previewProduct' src='<c:url value='/resources/img/products/${order.value.product.id}.jpg'/>'>
                                                </td>
                                                <td class="name">${order.value.product.name} </td>
                                                <td>
                                                    <input name="inputQuantity" class="inputQuantity" id="inputQuantity${order.value.product.id}" type="text" value="${order.value.quantity}" onkeyup="inputQuantityCheck(this);
                                                            addOrderCheckout(${order.value.product.id}, $(this).val());" onchange="inputQuantityCheck(this);
                                                                    addOrderCheckout(${order.value.product.id}, $(this).val());">
                                                    <script>
                                                        $("#inputQuantity${order.value.product.id}").TouchSpin({
                                                            verticalbuttons: true,
                                                            min: 1,
                                                            max: 999
                                                        });
                                                    </script>
                                                </td>
                                                <td class="productCartCost">
                                                    <fmt:formatNumber value="${order.value.product.cost}" type="currency"/>
                                                </td>
                                                <td >
                                                    <img class='img-delete' src='<c:url value='/resources/img/delete.png'/>' onclick="deleteOrderCheckout(${order.value.product.id})">
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                    <div id="cartFooter">
                                        <hr style="width: 100%; color: white; height: 1px; background-color:white; margin:5px;" />
                                        <span>
                                            <strong class="pull-right">
                                                Totale: <fmt:formatNumber value="${sum}" type="currency"/>                              
                                            </strong>
                                        </span>
                                        <span class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style='text-align: center;padding: 15px 0;'> 
                                            <a  id='buttonCancel' class='btn btn-default span7 text-center col-xs-3' href="/">Back</a>
                                            <button type='button' id='buttonProceed' class='btn btn-success span7 text-center col-xs-3 col-xs-offset-6' onclick="nextPhaseCheckout()">Proceed</button>
                                        </span>
                                    </div>
                                </c:when>
                                <c:otherwise> 
                                    <div id="cartBodyCheckout">
                                        <img class="img-cart" src="/resources/img/cartFull.png">
                                        <em>Your cart feels empty, sad and alone :( </em>
                                        <a  id="buttonCancel" class="btn btn-default span7 text-center col-xs-3 col-xs-offset-9" href="/">Back</a>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <footer class="">
            <div>
                <p id="copyright"class="text-muted">© 2015 Pizzeria Inc.</p>
                <ul id="links" class="list-inline">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#about" >About Us</a></li>
                    <li><a href="#about" onclick="showContact()">Our Location</a></li>
                    <li><a href="#about" onclick="showContact();">Contact Us</a></li>   
                </ul>        
            </div>
        </footer>
    </body>
</html>
