<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="modal fade" id="quantityFormModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background:steelblue;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                    <span aria-hidden="true">&times;</span></button>
                <p style="clear:both;"></p></div>
            <div class="modal-body">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                            <h4 class="modal-title" style="text-align:center;">${name}</h4><div class="panel-body flex-grow"><div class="fix">
                                    <img class="img-thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12 " src="/resources/img/products/${id}.jpg">
                                    <div class="desc">
                                        <p class="desc_content">
                                            <em>${ingredients}</em>
                                        </p>
                                    </div>
                                </div></div></div>
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6" style="text-align: left;">
                            <form method="POST" autocomplete="off" id="quantityForm' + ${id} + '" class="quantityForm">
                                <p><em>Price</em></p>
                                <strong class="cost">
                                    <fmt:formatNumber value="${cost}" type="currency"/>
                                </strong> 
                                <p style="margin-top: 20px;"><em>Quantity</em></p> 
                                <span><input name="quantityProduct" data-id="quantityProduct${id}" class="quantityProduct" type="text" maxlength="3" value="1" onchange="inputQuantityCheck(this, ${cost});" onkeyup="inputQuantityCheck(this, ${cost})">
                                    <script>$('input[data-id="quantityProduct${id}"]').TouchSpin({verticalbuttons: true, min: 1, max: 999});</script>
                                </span>
                                <span>
                                    <input name="idProduct" type="hidden" value="${id}">
                                </span>
                            </form>
                        </div>
                    </div>
                    <p class="total">Total: <fmt:formatNumber value="${cost}" type="currency"/> </p>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" id="addButton"  class="buttonQuantity btn btn-primary pull-right" onclick="addOrder(' + ${id} + ');" data-dismiss="modal">ADD</button>
            </div>
        </div> 
    </div>
</div>
