<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="modal fade" id="tabs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li id="loginLink" class="active"><a href="#login"  data-toggle="tab"><h4 >Login</h4></a></li>
                    <li id="registerLink"><a href="#register" data-toggle="tab"><h4 >Register</h4></a></li>
                </ul>
            </div>
            <div id="my-tab-content" class="tab-content">
                <div class="tab-pane active" id="login">
                    <div class="modal-body" >
                        <form class="well form-horizontal" id="form-login" action="<c:url value='j_spring_security_check' />" method="POST" role="form"  >
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" name="j_username" id="inputEmail" class="form-control" placeholder="Email address" required >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="j_password" id="inputPassword" class="form-control" placeholder="Password" required>
                                </div>
                            </div>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-success btn-md col-md-offset-7" type="submit" onclick="$('#form-login').submit();">Login</button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="register">
                    <div class="modal-body">
                        <form class="well form-horizontal" autocomplete="off" id="form-register" role="form" method="POST">
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="password1" id="inputPassword" class="form-control" placeholder="Password" required pattern="[a-zA-Z0-9]{6,}" title="Minimum 6 letters or numbers.">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confirmPassword" class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <input type="password" name="password2" id="confirmPassword" class=" form-control" placeholder="Confirm Password" required pattern="[a-zA-Z0-9]{6,}" title="Minimum 6 letters or numbers." >
                                </div>
                            </div>
                            <input type="submit" id="registerSubmit" value="submit" style="display:none;">			
                        </form>
                        <script>
                            $("#form-register").submit(function (e)
                            {
                                var postData = $(this).serializeArray();
                                $.ajax(
                                        {
                                            url: "/register.html",
                                            type: "POST",
                                            data: postData,
                                            dataType: "json",
                                            success: function (response) {
                                                $("#login").prepend('<div id ="success" class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> User successfully registered.Please login.</div>')
                                                $("#loginLink").addClass("active");
                                                $("#login").addClass("active");
                                                $("#registerLink").removeClass("active");
                                                $("#register").removeClass("active");
                                            },
                                            error: function ()
                                            {
                                                $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to register using this email and passwords.</div>').insertBefore("#form-register");

                                            }
                                        });
                                e.preventDefault(); //STOP default action
                                e.unbind();
                            });</script>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary btn-md col-md-offset-7" type="submit" onclick="registerSubmit();">Register</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
