<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="modal fade" id="insertModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background:steelblue;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                    <span aria-hidden="true">&times;</span></button>
                <p style="clear:both;"></p></div>

            <div class="modal-body">
                <form class="well form-horizontal" method="POST" action="" enctype="multipart/form-data" id="insertForm">
                    <div class="form-group">  
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" id="inputName" class=" form-control" placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">  
                        <label for="inputIngredients" class="col-sm-2 control-label">Ingredients</label>
                        <div class="col-sm-10">
                            <input type="text" name="ingredients" id="inputIngredients" class=" form-control" placeholder="pomodoro,mozzarella">
                        </div>
                    </div>
                    <div class="form-group">  
                        <label for="inputDescription" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea  name="description" id="inputDescription" class=" form-control" placeholder="Description..."></textarea>
                        </div>
                    </div>
                    <div class="form-group">  
                        <label for="inputCategory" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <input type="text" name="category" id="inputCategory" class=" form-control" placeholder="classic" value="classic">
                        </div>
                    </div>
                    <div class="form-group">  
                        <label for="inputPrice" class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-10">
                            <input type="text" name="price" id="inputPrice" class=" form-control" placeholder="4.00">
                        </div>
                    </div>

                    <div class="form-group">
                        <div id="myDropzone" class="dropzone">
                            <div class="dz-message"><b>Drag&Drop </b>the product preview here 
                                <br/>or <b>Click</b> to upload.
                            </div>
                        </div>     
                    </div>


                </form>
                <script>

                    
                       init("insertProduct");
                    
                </script>
            </div>
            <div class="modal-footer">
                <button type="button" class="buttonQuantity btn btn-primary pull-right" onclick="initP()">ADD</button>

            </div>
        </div> 
    </div>
</div>
