<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row" id='realBody'>
    <span id="titles">
        <div class="thirdTitle title">Cart revision</div>
        <div class="secondaryTitle title">Delivery details</div>
        <div class="mainTitle title">Complete order</div>
    </span>
    <div class="col-md-9 main well" style="z-index: 1;">
        <div class="well form-horizontal" id="checkout_complete">
            <div class="form-group">  
                <label class="col-sm-2">Name</label>
                <div class="col-sm-10">
                    <label >${user.name}  ${user.surname} </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">Phone</label>
                <div class="col-sm-10">
                    <label>${user.phone}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">Deliver by</label>
                <div class="col-sm-10">
                    <label>${deliveryDate}</label>
                </div>
            </div>
            <div class="form-group">
                <label  class="col-sm-2">Address</label>
                <div class="col-sm-10">
                    <label>${address.address}</label>
                </div>
            </div>
            <span class="col-md-12 col-xs-12" style='padding:0;'> 
                <a  id='buttonCancel' class='btn btn-default span7 text-center col-xs-3 col-md-3' href="/">Cancel</a>
                <button type='button' id='buttonProceed' class='btn btn-success span7 text-center col-md-3 col-md-offset-6 col-xs-5 col-xs-offset-4' onclick="completeCheckout();">Pay & Order</button>
            </span>
        </div>
    </div>
</div>