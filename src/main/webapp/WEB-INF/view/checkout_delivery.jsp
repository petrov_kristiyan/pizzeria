<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row" id='realBody'>
    <span id="titles">
        <div class="secondaryTitle title">Cart revision</div>
        <div class="mainTitle title">Delivery details</div>
        <div class="secondaryTitle title">Complete order</div>
    </span>
    <div class="col-md-9 main well" style="z-index: 1;">
        <form class="well form-horizontal" id="form-delivery" method="POST" action="">
            <div class="form-group">  
                <label class="col-sm-2">Name</label>
                <div class="col-sm-10">
                    <label >${user.name}  ${user.surname} </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">Phone</label>
                <div class="col-sm-10">
                    <label>${user.phone}</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2">Deliver by</label>
                <div class="col-sm-10">
                    <input id="datetimepicker" type="text" name="deliveryDate">
                </div>
            </div>

            <div class="form-group">
                <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                    <input type="text" name="address" value="${user.address.address}"id="inputAddress" onfocus="initAutocomplete()" class=" form-control" placeholder="Address" required>
                </div>
            </div>

            <script>
                var currentDate = new Date();
                currentDate.setTime(currentDate.getTime() + 30 * 60 * 1000);
                var logic = function (currentDateTime) {
                    // 'this' is jquery object datetimepicker
                    if (currentDateTime.getDate() == currentDate.getDate()) {
                        this.setOptions({
                            minTime: currentDate,
                            maxTime: "23:00"
                        });
                    } else {
                        this.setOptions({
                            minTime: '08:00',
                            maxTime: "23:00"
                        });
                    }
                };
                jQuery('#datetimepicker').datetimepicker({
                    minDate: 0,
                    onShow: logic,
                    onChangeDateTime: logic,
                    mask: true,
                    step: 30
                });


                $("#form-delivery").on('submit', function (e)
                {
                    e.preventDefault();
                    var postData = $(this).serializeArray();
                    $.ajax(
                            {
                                url: "deliveryAddress.html",
                                type: "POST",
                                data: postData,
                                dataType: "json",
                                success: function (response) {
                                    if (response.status == "ok") {
                                        nextPhaseCheckout();
                                    }
                                },
                                error: function ()
                                {
                                    $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to set the delivery details.</div>').insertBefore("#form-delivery");

                                }
                            });
                    //STOP default action
                });
            </script>

            <span class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style='text-align: center;'> 
                <a  id='buttonCancel' class='btn btn-default span7 text-center col-xs-3' href="/">Cancel</a>
                <button type='submit' id='buttonProceed' class='btn btn-success span7 text-center col-xs-3 col-xs-offset-6'>Proceed</button>
            </span>
        </form>
    </div>
</div>