<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <title>Pizzeria</title>

        <script src="<c:url value="/resources/script/jquery-2.1.3.min.js"/>"></script>  
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="http://www.techieat.com/international-telephone/build/css/intlTelInput.css">

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"/>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/jquery.bootstrap-touchspin.css"/>"/>

        <script src="<c:url value="/resources/script/script.js"/>"></script>


        <script src="<c:url value="/resources/script/jquery.bootstrap-touchspin.js"/>"></script>
        <script src="http://www.techieat.com/international-telephone/build/js/intlTelInput.js"></script>

    </head>
    <body>  
        <div class="container-fluid">

            <header class="navbar navbar-default">
                <div class="">
                    <div class="logo-header">
                        <a class="navbar-brand col-xs-1 " href="index">Pizzeria</a>
                        <c:if test="${user.role eq 'ROLE_ADMIN' }">
                            <a class="navbar-brand col-xs-5 " href="/admin/index">Admin Section</a>
                        </c:if>
                        <div id="navbar" class="pull-right"> 
                            <c:choose>
                                <c:when test="${empty user.email}">
                                    <button type="button" id="loginButton" class="btn  btn-success">Login</button>
                                    <label class="text-muted">OR</label>
                                    <button class="btn  btn-primary" id="registerButton" type="button" > Register</button>
                                </c:when>
                                <c:otherwise>
                                    <div class="pull-left" id="profile">
                                        <button id="profileButton"  style="background-image:url('<c:url value="/resources/img/user.png"/>');" ></button>
                                        <label style="vertical-align: bottom;">${user.email}</label>
                                        <ul class="list-group" id="profileMenu">
                                            <li class="list-group-item"><a href="profile.html">Edit Profile</a></li>
                                            <li class="list-group-item"><a href="orders.html">Orders</a></li>
                                            <li class="list-group-item btn btn-danger"><a href="logout.html" >Logout</a></li>
                                        </ul>                                  
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </header>  
            <div class="row" id='realBody'>
                <div class="col-md-12">
                    <img id="" class="col-md-2 img-responsive" src='<c:url value="/resources/img/user.png"/>' >
                    <div class="col-md-10  main" style="z-index: 1">
                        <div class="modal-header">
                            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                <li class="active" data-toggle="tab"><h4 >Profile Information</h4></li>
                            </ul>
                        </div>
                        <div id="my-tab-content" class="tab-content"> 
                            <div class="tab-pane active" id="editProfile">
                                <div class="modal-body">
                                    <form class="well form-horizontal" method="POST" action="" id="form-editProfile">
                                        <div class="form-group">  
                                            <label for="inputName" class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" value="${user.name}" name="name" id="inputName" class=" form-control" placeholder="Name" required pattern="[a-zA-Z]{3,}" title="Minimum 3 letters.">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSurname" class="col-sm-2 control-label">Surname</label>
                                            <div class="col-sm-10">
                                                <input type="text" value="${user.surname}" name="surname" id="inputSurname" class=" form-control " placeholder="Surname" required pattern="[a-zA-Z]{3,}" title="Minimum 3 letters.">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                                            <div class="col-sm-10">
                                                <input type="text" value="${user.address.address}" name="address" id="inputAddress" class=" form-control" onfocus="initAutocomplete()" placeholder="Address" required>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                                            <div class="col-sm-10">
                                                <input type="tel" value="${user.phone}" name="phone" id="inputPhone" class=" form-control bfh-phone" placeholder="Phone number" required>
                                                <span class=" hide glyphicon form-control-feedback" id="inputPhoneValidation"></span>
                                                <script>

                                                    $("#inputPhone").intlTelInput({
                                                        autoFormat: true,
                                                        autoHideDialCode: false,
                                                        defaultCountry: "auto",
                                                        utilsScript: "http://www.techieat.com/international-telephone/lib/libphonenumber/build/utils.js"
                                                    });
                                                    var telInput = $("#inputPhone");
                                                    telInput.blur(function () {
                                                        if ($.trim(telInput.val())) {
                                                            if (!telInput.intlTelInput("isValidNumber")) {
                                                                telInput.closest("div[class^='form-group']").removeClass("has-success").addClass("has-error");
                                                                $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-ok").addClass("glyphicon-remove");
                                                            }
                                                            else {
                                                                telInput.closest("div[class^='form-group']").removeClass("has-error").addClass("has-success");
                                                                $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-remove").addClass("glyphicon-ok");
                                                            }
                                                        }
                                                    });
                                                    telInput.keyup(function () {
                                                        if (!telInput.intlTelInput("isValidNumber")) {
                                                            telInput.closest("div[class^='form-group']").removeClass("has-success").addClass("has-error");
                                                            $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-ok").addClass("glyphicon-remove");
                                                        }
                                                        else {
                                                            telInput.closest("div[class^='form-group']").removeClass("has-error").addClass("has-success");
                                                            $("#inputPhoneValidation").removeClass("hide").removeClass("glyphicon-remove").addClass("glyphicon-ok");
                                                        }
                                                    });
                                                    $("#form-editProfile").on('submit', function (e)
                                                    {
                                                        e.preventDefault();
                                                        var postData = $(this).serializeArray();
                                                        $.ajax(
                                                                {
                                                                    url: "editProfile.html",
                                                                    type: "POST",
                                                                    data: postData,
                                                                    dataType: "json",
                                                                    success: function (response) {
                                                                        $('<div id ="success" class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> Profile updated.</div>').insertBefore("#form-editProfile");
                                                                    },
                                                                    error: function ()
                                                                    {
                                                                        $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to update profile.</div>').insertBefore("#form-editProfile");

                                                                    }
                                                                });
                                                        //STOP default action
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" value="${user.email}" name="email" id="inputEmail" class="form-control" placeholder="Email address">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" name="password1" id="inputPassword" class="form-control" placeholder="Password"  pattern="[a-zA-Z0-9]{6,}" title="Minimum 6 letters or numbers.">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="confirmPassword" class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <input type="password" name="password2" id="confirmPassword" class=" form-control" placeholder="Confirm Password"  pattern="[a-zA-Z0-9]{6,}" title="Minimum 6 letters or numbers." >
                                            </div>
                                        </div>
                                        <input type="submit" id="editProfileSubmit" value="submit" style="display:none;">			
                                    </form>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary btn-md col-md-offset-7" type="submit" onclick="$('#editProfileSubmit').click();">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-2 sidebar" style="bottom: 40px; z-index: 0;">
                    <img id="logo" src="<c:url value="/resources/img/pizzeria.jpg"/>"  alt="Pizzeria" width="162" height="136">

                </div>
                <div id="showR"></div>
                <jsp:include page="info.jsp"></jsp:include>
                <div id="push"></div>

            </div>
        </div>
<jsp:include page="footer.jsp" />