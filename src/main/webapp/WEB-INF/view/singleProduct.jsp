<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="col-md-4 col-sm-5 col-xs-12" id="product${product.id}">
    <div class="panel panel-default flex-col">
        <strong class="panel-heading">
            ${product.name}  
            <img class='img-delete pull-right' src='<c:url value='/resources/img/delete.png'/>' onclick="deleteProduct(${product.id})">
            <img class='img-edit pull-left' src='<c:url value='/resources/img/edit.png'/>' onclick="showEditProduct(${product.id})">
        </strong>

        <div class="panel-body flex-grow">
            <div class="fix">
                <img class='img-thumbnail col-lg-12 col-md-12 col-sm-12 col-xs-12 ' src='<c:url value='/resources/img/products/${product.id}.jpg?time=${time}'/>'>
                <div class="desc">
                    <p class="desc_content"><em>${product.ingredients}</em></p>
                </div>
            </div>
        </div> 
        <div class="panel-footer clearfix"> 
            <div class='row vertical-align'>
                <div class="col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3 col-md-6 col-md-offset-3">
                    <div class='cost'>                                               
                        <fmt:formatNumber value="${product.cost}"  type="currency"/>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-3 col-md-3">

                </div>
            </div>
        </div>
    </div>
</div>