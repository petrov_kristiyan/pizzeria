<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <title>Pizzeria</title>

        <script src="<c:url value="/resources/script/jquery-2.1.3.min.js"/>"></script>  
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="http://www.techieat.com/international-telephone/build/css/intlTelInput.css">

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/style.css"/>"/>

        <link type="text/css" rel="stylesheet" href="<c:url value="/resources/css/jquery.bootstrap-touchspin.css"/>"/>

        <script src="<c:url value="/resources/script/script.js"/>"></script>


        <script src="<c:url value="/resources/script/jquery.bootstrap-touchspin.js"/>"></script>
        <script src="http://www.techieat.com/international-telephone/build/js/intlTelInput.js"></script>

    </head>
    <body>  
        <div class="container-fluid">
            <header class="navbar navbar-default">
                <div class="">
                    <div class="logo-header">
                        <a class="navbar-brand col-xs-1 " href="index">Pizzeria</a>
                        <c:if test="${user.role eq 'ROLE_ADMIN' }">
                            <a class="navbar-brand col-xs-5 " href="/admin/index">Admin Section</a>
                        </c:if>
                        <div id="navbar" class="pull-right"> 
                            <c:choose>
                                <c:when test="${empty user.email}">
                                    <button type="button" id="loginButton" class="btn  btn-success">Login</button>
                                    <label class="text-muted">OR</label>
                                    <button class="btn  btn-primary" id="registerButton" type="button" > Register</button>
                                </c:when>
                                <c:otherwise>
                                    <div class="pull-left" id="profile">
                                        <button id="profileButton"  style="background-image:url('<c:url value="/resources/img/user.png"/>');" ></button>
                                        <label style="vertical-align: bottom;">${user.email}</label>
                                        <ul class="list-group" id="profileMenu">
                                            <li class="list-group-item"><a href="profile.html">Edit Profile</a></li>
                                            <li class="list-group-item"><a href="orders.html">Orders</a></li>
                                            <li class="list-group-item btn btn-danger"><a href="logout.html" >Logout</a></li>
                                        </ul>                                  
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </header>
            <div class="row" id='realBody'>
                <div class="col-md-9 col-md-offset-2 main" style="z-index: 1">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#pending">Pending orders</a></li>
                        <li>
                            <a data-toggle="tab" href="#received">Received Orders</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="pending" class="tab-pane fade in active">
                            <div class="orderHeader"></div>
                            <c:forEach var="order" items="${user.history}"> 
                                <c:if test="${order.received == false}">
                                    <c:set var='sum' value="0"/>
                                    <div class="well" id="order${order.id}">
                                        <table class="table tableCategory table-striped">
                                            <thead>
                                                <tr class="">

                                                    <th class="col-md-2"></th>
                                                    <th class="col-md-2">Name</th>
                                                    <th class="col-md-2">Category</th>
                                                    <th class="col-md-2">Quantity</th>
                                                    <th class="col-md-2">Cost</th>
                                                    <th class="col-md-2">Total cost</th>

                                                </tr>
                                            </thead>

                                            <div class="well text-center"> Order #${order.id} </div>
                                            <tbody>
                                                <c:forEach var="userproduct" items="${order.products}"> 
                                                    <c:set var='sum' value="${0+sum+userproduct.quantity*userproduct.product.cost}"/>
                                                    <tr >
                                                        <td class="col-md-2"> 
                                                            <img class='img-thumbnail img-previewProduct' src='<c:url value='/resources/img/products/${userproduct.product.id}.jpg'/>'>
                                                        </td>
                                                        <td class="col-md-2">${userproduct.product.name}</td>
                                                        <td class="col-md-2">${userproduct.product.category}</td>
                                                        <td class="col-md-2">${userproduct.quantity}</td>
                                                        <td class="col-md-2">
                                                            <fmt:formatNumber value="${userproduct.product.cost}" type="currency"/>

                                                        </td>
                                                        <td class="col-md-2">
                                                            <fmt:formatNumber value="${userproduct.product.cost*userproduct.quantity}" type="currency"/>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <p class="orderFooter">Deliver by : ${order.deliveryDate}</p>
                                        <p class="orderFooter"> Date shipped : ${order.shipped}</p>
                                        <p class="orderFooter"> Address : ${order.address.address}</p>
                                        <span>
                                            <hr/>
                                            <button class="btn btn-success col-md-2" id="buttonReceivedOrder" onclick="confirmDelivery(${order.id})">Received</button>
                                            <button class="btn btn-default col-md-2 col-md-offset-1" id="buttonDeleteOrder" onclick="deletePendingOrder(${order.id})">Delete Order</button>
                                            <strong class="col-md-offset-5">
                                                Total: <fmt:formatNumber value="${sum}" type="currency"/>                              
                                            </strong>
                                        </span>
                                    </div>
                                </c:if>
                            </c:forEach>


                        </div>
                        <div id="received" class="tab-pane fade">


                            <div class="orderHeader"></div>
                            <c:forEach var="order" items="${user.history}"> 
                                <c:if test="${order.received ==true}">
                                    <c:set var='sum' value="0"/>
                                    <div class="well" id="order${id}">
                                        <table class="table tableCategory table-striped">
                                            <thead>
                                                <tr class="">

                                                    <th class="col-md-2"></th>
                                                    <th class="col-md-2">Name</th>
                                                    <th class="col-md-2">Category</th>
                                                    <th class="col-md-2">Quantity</th>
                                                    <th class="col-md-2">Cost</th>
                                                    <th class="col-md-2">Total cost</th>

                                                </tr>
                                            </thead>

                                            <div class="well text-center"> Order #${order.id} </div>
                                            <tbody>
                                                <c:forEach var="userproduct" items="${order.products}"> 
                                                    <c:set var='sum' value="${0+sum+userproduct.quantity*userproduct.product.cost}"/>
                                                    <tr >
                                                        <td class="col-md-2"> 
                                                            <img class='img-thumbnail img-previewProduct' src='<c:url value='/resources/img/products/${userproduct.product.id}.jpg'/>'>
                                                        </td>
                                                        <td class="col-md-2">${userproduct.product.name}</td>
                                                        <td class="col-md-2">${userproduct.product.category}</td>
                                                        <td class="col-md-2">${userproduct.quantity}</td>
                                                        <td class="col-md-2">
                                                            <fmt:formatNumber value="${userproduct.product.cost}" type="currency"/>

                                                        </td>
                                                        <td class="col-md-2">
                                                            <fmt:formatNumber value="${userproduct.product.cost*userproduct.quantity}" type="currency"/>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                        <p class="orderFooter">Deliver by : ${order.deliveryDate}</p>
                                        <p class="orderFooter"> Date shipped : ${order.shipped}</p>
                                        <p class="orderFooter"> Address : ${order.address.address}</p>
                                        <span>
                                            <hr/>
                                            <strong class="col-md-offset-10">
                                                Total: <fmt:formatNumber value="${sum}" type="currency"/>                              
                                            </strong>
                                        </span>
                                    </div>
                                </c:if>
                            </c:forEach>


                        </div>
                    </div>

                    <div id="showR"></div>
                    <jsp:include page="info.jsp"></jsp:include>
                    <div id="push"></div>

                </div>
            </div>
<jsp:include page="footer.jsp" />
