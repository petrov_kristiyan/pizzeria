/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var patternName = /[a-zA-Z0-9]{3,}/; //name at least 3 chars
var patternPrice = /^(\d+(?:[\.\,]\d{1,2})?)$/; //positive 2 decimal number
var patternWordList = /[^,]+/; // word list comma separated
var myDropzone;
var fileUp = false;
var productId;
function init(urlAction) {
    myDropzone = new Dropzone(".dropzone", {
        url: urlAction,
        autoProcessQueue: false,
        maxFiles: 1,
        acceptedFiles: "image/*",
        init: function () {
            this.on("addedfile", function (file) {
                fileUp = true;
            });
        },
        maxfilesexceeded: function (file) {
            this.removeAllFiles();
            this.addFile(file);
        },
        sending: function (data, xhr, formData) {
            formData.append("file", data, data.name);
            if (productId != null) {
                formData.append("inputId", productId);
            }
            formData.append("inputName", $('#inputName').val());
            formData.append("inputIngredients", $('#inputIngredients').val());
            formData.append("inputCategory", $('#inputCategory').val());
            formData.append("inputDescription", $('#inputDescription').val());
            formData.append("inputPrice", parseFloat($('#inputPrice').val().replace(",", ".")));
        },
        success: function (file, response) {
            $("#error").remove();
            if (productId != null) {
                $("#product" + productId).replaceWith(response);
                BootstrapDialog.confirm({
                    title: 'Success',
                    message: 'The product was modified',
                    type: BootstrapDialog.TYPE_SUCCESS, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    buttonLabel: 'OK', // <-- Default value is 'OK',
                    callback: function (result) {
                        $("#editModal").modal('hide');
                    }
                });
            }
            else {
                $("#columns").append(response);
                $("#insertModal").modal('hide');
            }
        },
        error: function (file, errorMessage) {
            $("#error").remove();
            $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Error insert of product failed. Check value and format of the fields</div>').insertBefore("#insertForm");
        },
        addRemoveLinks: true

    });
}
$(document).ready(function () {
    $("#profileButton").on('click', function () {
        $("#profile").toggleClass("user-profile");
        $("#profileMenu").css("width", $("#profile").outerWidth() + "px");
        $("#profileMenu").slideToggle("fast", function () {
        });
    });
});
function errorMessages() {
    $("#error").remove();
    $(".dz-progress").css({'display': 'block!important'});

    if (!patternName.test($("#inputName").val())) {
        $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Name missing or format invalid ( Minimum 3 Characters - letters and numbers only ) . </div>').insertBefore("#insertForm");
    }
    else
    if (!patternWordList.test($("#inputIngredients").val())) {
        $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Ingredients list missing or format invalid ( ex: pomodoro,mozzarella ). </div>').insertBefore("#insertForm");
    }
    else
    if (!patternWordList.test($("#inputCategory").val())) {
        $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Category missing or format invalid ( ex: classic ). </div>').insertBefore("#insertForm");
    }
    else
    if (!patternPrice.test($("#inputPrice").val())) {
        $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Price missing or format invalid ( correct format is ex: 4.00 ). </div>').insertBefore("#insertForm");
    }
    else
    if (!fileUp) {
        $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> File missing. </div>').insertBefore("#insertForm");
    }
    else
        return true;
}


function initP() {
    if (errorMessages() && fileUp) {
        myDropzone.processQueue();
    }

}

function showModal() {
    $("#insertModal").remove();
    $.ajax({
        type: "GET",
        url: "showInsertProduct.html",
        success: function (response) {
            $("body").prepend(response);
            $('#insertModal').modal('toggle');
            $('#insertModal').modal('show');
        },
        error: function () {
        }
    });
}
function deleteProduct(idProduct) {
    BootstrapDialog.confirm({
        title: 'Delete product',
        message: 'The product will be DELETED',
        type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: 'Cancel',
        btnOKLabel: 'OK', // <-- Default value is 'OK',
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {

                $.ajax({
                    type: "POST",
                    url: "deleteProduct",
                    data: {productId: idProduct},
                    success: function (response) {
                        $("#product" + idProduct).remove();
                        BootstrapDialog.show({
                            title: 'Success',
                            message: 'The product was deleted',
                            type: BootstrapDialog.TYPE_SUCCESS, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                            closable: true, // <-- Default value is false
                            draggable: true, // <-- Default value is false
                            buttons: [{
                                    label: 'Close',
                                    action: function (dialogItself) {
                                        dialogItself.close();
                                    }
                                }],
                            error: function () {
                                BootstrapDialog.show({
                                    title: 'Error',
                                    message: 'Deletion failed',
                                    type: BootstrapDialog.TYPE_DANGER, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                                    closable: true, // <-- Default value is false
                                    draggable: true, // <-- Default value is false
                                    buttons: [{
                                            label: 'Close',
                                            action: function (dialogItself) {
                                                dialogItself.close();
                                            }
                                        }]
                                });
                            }
                        });
                    }

                });
            }
        }
    });
}


function showEditProduct(idProduct) {
    productId = idProduct;
    $("#editModal").remove();
    $.ajax({
        type: "GET",
        data: "idProduct=" + idProduct,
        url: "showEditProduct.html",
        success: function (response) {
            $("body").prepend(response);
            $('#editModal').modal('toggle');
            $('#editModal').modal('show');
        },
        error: function () {
        }
    });
}

var map;
var myLatlng;
function initialize() {
    myLatlng = new google.maps.LatLng(45.09041, 7.66093);
    var mapProp = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: "Pizzeria"
    });

}

var autocomplete;


function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('inputAddress')),
            {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
}


google.maps.event.addDomListener(window, 'load', initialize);
function showContact() {
    if ($("#contact").css("display") == "block") {
        $('#contact').hide();
    }
    else {
        $('#contact').show();
        google.maps.event.trigger(map, 'resize');
        map.setCenter(myLatlng);
    }
}
