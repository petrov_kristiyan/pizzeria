var map;
var myLatlng;
function initialize() {
    myLatlng = new google.maps.LatLng(45.09041, 7.66093);
    var mapProp = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: "Pizzeria"
    });

}

var autocomplete;


function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('inputAddress')),
            {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
}


google.maps.event.addDomListener(window, 'load', initialize);
function showContact() {
    if ($("#contact").css("display") == "block") {
        $('#contact').hide();
    }
    else {
        $('#contact').show();
        google.maps.event.trigger(map, 'resize');
        map.setCenter(myLatlng);
    }
}
$(document).ready(function () {

    $("#loginButton").click(function () {
        if (!$("#tabs").length) {
            $.ajax({
                type: "GET",
                url: "showEnter",
                success: function (response) {
                    $("body").append(response);
                    $("#loginLink").addClass("active");
                    $("#login").addClass("active");
                    $("#registerLink").removeClass("active");
                    $("#register").removeClass("active");
                    $('#tabs').modal('toggle');
                    $('#tabs').modal('show');
                },
                error: function () {
                }
            });

        }
        $("#loginLink").addClass("active");
        $("#login").addClass("active");
        $("#registerLink").removeClass("active");
        $("#register").removeClass("active");
        $('#tabs').modal('toggle');
        $('#tabs').modal('show');
    });

    $("#cartButton").on('click', function () {
        $(".cartContent").fadeToggle("fast");
    });

    $("#profileButton").on('click', function () {
        $("#profile").toggleClass("user-profile");
        $("#profileMenu").css("width", $("#profile").outerWidth() + "px");
        $("#profileMenu").slideToggle("fast", function () {
        });
    });



    $(document).mouseup(function (e)
    {
        var container = $(".cartContent");

        if (!container.is(e.target) && container.has(e.target).length === 0 && $('#cart').has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }

        var container = $("#profileMenu");

        if (!container.is(e.target) && container.has(e.target).length === 0 && $('#profile').has(e.target).length === 0) // ... nor a descendant of the container
        {
            if ($("#profile").hasClass("user-profile")) {
                $("#profile").toggleClass("user-profile");
            }
            container.hide();
        }

    });
    $('#cartBody').hover(function () {
        $("body").css("overflow", "hidden");
    }, function () {
        $("body").css("overflow", "auto");
    });
    $("#registerButton").click(function () { // when #showhidecomment is clicked
        if (!$("#tabs").length) {
            $.ajax({
                type: "GET",
                url: "showEnter",
                success: function (response) {
                    $("body").append(response);
                    $("#register").addClass("active");
                    $("#registerLink").addClass("active");
                    $("#login").removeClass("active");
                    $("#loginLink").removeClass("active");
                    $('#tabs').modal('toggle');
                    $('#tabs').modal('show');
                },
                error: function () {
                }
            });
        }
        $("#register").addClass("active");
        $("#registerLink").addClass("active");
        $("#login").removeClass("active");
        $("#loginLink").removeClass("active");
        $('#tabs').modal('toggle');
        $('#tabs').modal('show');

    });
    $("#form-register").submit(function (e)
    {
        var postData = $(this).serializeArray();
        $.ajax(
                {
                    url: "/register.html",
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    success: function (response) {
                        $("#login").prepend('<div id ="success" class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> User successfully registered.Please login.</div>')
                        $("#loginLink").addClass("active");
                        $("#login").addClass("active");
                        $("#registerLink").removeClass("active");
                        $("#register").removeClass("active");
                    },
                    error: function ()
                    {
                        $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to register using this email and passwords.</div>').insertBefore("#form-register");

                    }
                });
        e.preventDefault(); //STOP default action
        e.unbind();
    });
    updateFooterPosition();

});

function updateFooterPosition() {
    var docHeight = $(window).height();
    var footerHeight = $('footer').height();
    var footerTop = $('footer').position().top + footerHeight;

    if (footerTop < docHeight) {
        $('footer').css('margin-top', 10 + (docHeight - footerTop) + 'px');
    }
}

function registerSubmit() {
    $("#error").remove();
    $("#success").remove();

    $("#form-register").submit();


}

function updateSpinner(move, id)
{
    var value = $("input[data-id='quantity" + id + "']").val();
    if (move == "down") {
        if (value > 1)
            value--;
    } else {
        if (value < 999)
            value++;
    }
    $("input[data-id='quantity" + id + "']").val(value);
}


function deleteOrder(id) {

    $("#error").remove();
    $(this).popover('hide');
    $.ajax({
        type: "POST",
        data: "idProduct=" + id,
        url: "deleteOrder.html",
        dataType: "json",
        success: function (response) {
            var json = response;
            var totalSum = 0.0;
            var totalNum = 0;
            $("#cartContent").empty();
            if (json.length == 0) {
                $("#cartContent").append('<div id="cartBody"><img class="img-cart" src="/resources/img/cartFull.png"><em>Your cart feels empty, sad and alone :( </em></div><div class="triangle"></div>');
                $("#cartSum").html("&euro; " + totalSum.toFixed(2).toString().replace(".", ","));
                $("#cartProductsNumber").html(json.length);
                $("#cartProductsNumber").hide();
            }
            else {
                $("#cartContent").append('<table id="cartBody">');
                $("#cartSum").empty();
                for (var i in json)
                {
                    totalSum += (json[i].quantity * json[i].cost);
                    totalNum += (json[i].quantity)
                    $("#cartBody").append(fillCartBody(json[i]));
                }
                $("#cartContent").append('</table><div id="cartFooter">');
                $("#cartFooter").append('<hr style="width: 100%; color: white; height: 1px; background-color:white; margin:5px;" /><span><strong class="pull-right">Totale: &euro; ' + totalSum.toFixed(2).toString().replace(".", ",") + '</strong> </span><span class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="text-align: center;padding: 15px 0;"> <button type="submit" id="buttonCheckout" class="btn btn-danger span7 text-center" onclick="">Checkout</button></span></div><div class="triangle"></div>');
                $("#cartSum").html("&euro; " + totalSum.toFixed(2).toString().replace(".", ","));
                $("#cartProductsNumber").html(totalNum);
                $("#cartProductsNumber").show();

            }
            $('#cartBody').hover(function () {
                $("body").css("overflow", "hidden");
            }, function () {
                $("body").css("overflow", "auto");
            });
        },
        error: function () {
            $('header').after('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to delete item from the Cart.</div>');
        }
    });
}

function addOrder(id, quantity) {
    var url = "updateOrder.html";
    if (typeof quantity === "undefined") {
        quantity = $("input[name='quantityProduct']").val();
        url = "addOrder.html";
    }
    $("#error").remove();
    $(this).popover('hide');
    $.ajax({
        type: "POST",
        data: "quantityProduct=" + quantity + "&idProduct=" + id,
        url: url,
        dataType: "json",
        success: function (response) {
            var json = response;
            var totalSum = 0.0;
            var totalNum = 0;
            $("#cartContent").empty();
            $("#cartContent").append('<table id="cartBody">');
            $("#cartSum").empty();
            for (var i in json)
            {
                totalSum += (json[i].quantity * json[i].cost);
                totalNum += (json[i].quantity)
                if (json[i].id == id) {
                    $("#cartBody").append(fillCartBody(json[i], "autofocus"));
                    $("#cartBody").append("<script>$('#inputQuantity" + id + "').focus();$('#inputQuantity" + id + "').mouseleave(function() {$('#inputQuantity" + id + "').blur();});</script>");
                }
                else
                    $("#cartBody").append(fillCartBody(json[i], ""));
            }
            $("#cartContent").append('</table><div id="cartFooter">');
            $("#cartFooter").append('<hr style="width: 100%; color: white; height: 1px; background-color:white; margin:5px;" /><span><strong class="pull-right">Totale: &euro; ' + totalSum.toFixed(2).toString().replace(".", ",") + '</strong> </span><span class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="text-align: center;padding: 15px 0;"> <button type="submit" id="buttonCheckout" class="btn btn-danger span7 text-center" onclick="">Checkout</button></span></div><div class="triangle"></div>');
            $("#cartSum").html("&euro; " + totalSum.toFixed(2).toString().replace(".", ","));
            $("#cartProductsNumber").html(totalNum);
            $("#cartProductsNumber").show();
            $('#cartBody').hover(function () {
                $("body").css("overflow", "hidden");
            }, function () {
                $("body").css("overflow", "auto");
            });
        },
        error: function () {
            $('header').after('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to add item to the Cart.</div>');
        }
    });


}

function inputQuantityCheck(id, cost) {
    var value = $(id).val();
    if (value.length === 1) {
        var regReplace = /[^1-9]/g;
        $(id).val($(id).val().replace(regReplace, '1'));
    }
    else {
        var regReplace = /[^0-9]*/g;
        $(id).val(value.replace(regReplace, ''));
    }
    if (cost != null) {
        var total = ($(id).val() * cost).toFixed(2).toString().replace(".", ",");

        if (total === "0,00") {
            $("#addButton").prop("disabled", true);
        }
        else
            $("#addButton").prop("disabled", false);
        $(".total").html("Total: &euro; " + total);
    }
}

function fillCartBody(order) {
    return '<tr class="rowCart" >\n\
            <td>\n\
            <img class="img-thumbnail img-previewProduct" src="/resources/img/products/' + order.id + '.jpg">\n\
            </td>\n\
           <td> ' + order.name + ' </td>\n\\n\
           <td><input id="inputQuantity' + order.id + '"class="inputQuantity" type = "text" value="' + order.quantity + '"  onkeyup = "inputQuantityCheck(this);addOrder(' + order.id + ',$(this).val());" onchange="inputQuantityCheck(this);addOrder(' + order.id + ',$(this).val());" name = "demo_vertical" >\n\
           <script>$("#inputQuantity' + order.id + '").TouchSpin({\n\
           verticalbuttons: true,\n\
           min: 1,\n\
           max: 999\n\
            });</script> </td>\n\
            <td class = "productCartCost" >&euro; ' + order.cost.toFixed(2).toString().replace(".", ",") + ' </td>\n\
           <td>\n\
           <img class = "img-delete" src="/resources/img/delete.png" onclick="deleteOrder(' + order.id + ')">\n\
            </td></tr>';
}
function showModal(id) {
    $("#quantityFormModal").remove();
    $.ajax({
        type: "GET",
        data: "idProduct=" + id,
        url: "showAddProduct.html",
        success: function (response) {
            $("body").prepend(response);
            $('#quantityFormModal').modal('toggle');
            $('#quantityFormModal').modal('show');
        },
        error: function () {
        }
    });
}

function logout() {
    $.ajax({
        type: "POST",
        url: "logout.html",
        dataType: "json",
        success: function (response) {
            window.location.assign(response.link);
        },
        error: function () {

        }
    });
}




function deleteOrderCheckout(id) {

    $("#error").remove();
    $(this).popover('hide');
    $.ajax({
        type: "POST",
        data: "idProduct=" + id,
        url: "deleteOrder.html",
        dataType: "json",
        success: function (response) {
            var json = response;
            var totalSum = 0.0;
            var totalNum = 0;
            $("#cartContent").empty();
            if (json.length == 0) {
                $("#cartContent").append('<div id="cartBodyCheckout"><img class="img-cart" src="/resources/img/cartFull.png"><em>Your cart feels empty, sad and alone :( </em> <a id="buttonCancel" class="btn btn-default span7 text-center col-xs-3 col-xs-offset-9" href="/">Back</a></div>');
                $("#cartSum").html("&euro; " + totalSum.toFixed(2).toString().replace(".", ","));
                $("#cartProductsNumber").html(json.length);
                $("#cartProductsNumber").hide();
            }
            else {
                $("#cartContent").append('<table id="cartBodyCheckout">');
                $("#cartSum").empty();
                for (var i in json)
                {
                    totalSum += (json[i].quantity * json[i].cost);
                    totalNum += (json[i].quantity);
                    $("#cartBodyCheckout").append(fillCartBodyCheckout(json[i]));
                }
                $("#cartContent").append('</table><div id="cartFooter">');
                $("#cartFooter").append("<hr style='width: 100%; color: white; height: 1px; background-color:white; margin:5px;' /><span><strong class='pull-right'>Totale: &euro; " + totalSum.toFixed(2).toString().replace(".", ",") + "</strong> </span><span class='col-md-12 col-lg-12 col-sm-12 col-xs-12' style='text-align: center;padding: 15px 0;'><a id='buttonCancel' class='btn btn-default span7 text-center col-xs-3' href='/'>Cancel</a><button type='submit' id='buttonProceed' class='btn btn-success span7 text-center col-xs-3 col-xs-offset-6' onclick='nextPhaseCheckout()'>Proceed</button></span></div>");
                $("#cartSum").html("&euro; " + totalSum.toFixed(2).toString().replace(".", ","));
                $("#cartProductsNumber").html(totalNum);
                $("#cartProductsNumber").show();

            }
            $('#cartBodyCheckout').hover(function () {
                $("body").css("overflow", "hidden");
            }, function () {
                $("body").css("overflow", "auto");
            });
        },
        error: function () {
            $('header').after('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to delete item from the Cart.</div>');
        }
    });
}

function addOrderCheckout(id, quantity) {
    var url = "updateOrder.html";
    if (typeof quantity === "undefined") {
        quantity = $("input[name='quantityProduct']").val();
        url = "addOrder.html";
    }
    $("#error").remove();
    $(this).popover('hide');
    $.ajax({
        type: "POST",
        data: "quantityProduct=" + quantity + "&idProduct=" + id,
        url: url,
        dataType: "json",
        success: function (response) {
            var json = response;
            var totalSum = 0.0;
            var totalNum = 0;
            $("#cartContent").empty();
            $("#cartContent").append('<table id="cartBodyCheckout">');
            $("#cartSum").empty();
            for (var i in json)
            {
                totalSum += (json[i].quantity * json[i].cost);
                totalNum += (json[i].quantity)
                if (json[i].id == id) {
                    $("#cartBodyCheckout").append(fillCartBodyCheckout(json[i], "autofocus"));
                    $("#cartBodyCheckout").append("<script>$('#inputQuantity" + id + "').focus();$('#inputQuantity" + id + "').mouseleave(function() {$('#inputQuantity" + id + "').blur();});</script>");
                }
                else
                    $("#cartBodyCheckout").append(fillCartBodyCheckout(json[i], ""));
            }
            $("#cartContent").append('</table><div id="cartFooter">');
            $("#cartFooter").append("<hr style='width: 100%; color: white; height: 1px; background-color:white; margin:5px;' /><span><strong class='pull-right'>Totale: &euro; " + totalSum.toFixed(2).toString().replace(".", ",") + "</strong> </span><span class='col-md-12 col-lg-12 col-sm-12 col-xs-12' style='text-align: center;padding: 15px 0;'><a id='buttonCancel' class='btn btn-default span7 text-center col-xs-3' href='/' >Back</a><button type='submit' id='buttonProceed' class='btn btn-success span7 text-center col-xs-3 col-xs-offset-6' onclick='nextPhaseCheckout()'>Proceed</button></span></div>");
            $("#cartSum").html("&euro; " + totalSum.toFixed(2).toString().replace(".", ","));
            $("#cartProductsNumber").html(totalNum);
            $("#cartProductsNumber").show();
            $('#cartBodyCheckout').hover(function () {
                $("body").css("overflow", "hidden");
            }, function () {
                $("body").css("overflow", "auto");
            });
        },
        error: function () {
            $('header').after('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to add item to the Cart.</div>');
        }
    });


}
function fillCartBodyCheckout(order) {
    return '<tr class="rowCart" >\n\
            <td>\n\
            <img class="img-thumbnail img-previewProduct" src="/resources/img/products/' + order.id + '.jpg">\n\
            </td>\n\
           <td class="name"> ' + order.name + ' </td>\n\\n\
           <td><input id="inputQuantity' + order.id + '"class="inputQuantity" type = "text" value="' + order.quantity + '"  onkeyup = "inputQuantityCheck(this);addOrderCheckout(' + order.id + ',$(this).val());" onchange="inputQuantityCheck(this);addOrderCheckout(' + order.id + ',$(this).val());" name = "demo_vertical" >\n\
           <script>$("#inputQuantity' + order.id + '").TouchSpin({\n\
           verticalbuttons: true,\n\
           min: 1,\n\
           max: 999\n\
            });</script> </td>\n\
            <td class = "productCartCost" >&euro; ' + order.cost.toFixed(2).toString().replace(".", ",") + ' </td>\n\
           <td>\n\
           <img class = "img-delete" src="/resources/img/delete.png" onclick="deleteOrderCheckout(' + order.id + ')">\n\
            </td></tr>';
}
function nextPhaseCheckout() {

    $.ajax({
        type: "POST",
        url: "nextPhaseCheckout.html",
        success: function (response) {
            $("#realBody").replaceWith(response);
        },
        error: function () {

        }
    });
}
function completeCheckout() {
    $.ajax(
            {
                url: "completeCheckout.html",
                type: "GET",
                dataType: "json",
                success: function (response) {
                    location.href = "orders";
                },
                error: function ()
                {
                    $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to complete checkout</div>').insertBefore("#checkout_complete");
                }
            });
}
function deletePendingOrder(id) {
    $.ajax(
            {
                url: "deleteOrder.html?order=" + id,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    $("#order" + id).remove();
                },
                error: function ()
                {
                    $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to complete checkout</div>').insertBefore("#checkout_complete");
                }
            });
}
function confirmDelivery(id) {
    $.ajax(
            {
                url: "confirmDelivery.html?order=" + id,
                type: "GET",
                dataType: "json",
                success: function (response) {
                    var temp = $("#order" + id);
                    $("#order" + id).remove();

                    temp.insertAfter("#received .orderHeader");
                    $("#buttonReceivedOrder").remove();
                    $("#buttonDeleteOrder").remove();
                    updateFooterPosition();

                },
                error: function ()
                {
                    $('<div id ="error" class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Failed to complete checkout</div>').insertBefore("#checkout_complete");
                }
            });
}
